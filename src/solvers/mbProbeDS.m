%
%   ODE run-tim callback. User's can add custom code here.
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function status = mbProbeDS(t,y,flag)

    if ~isempty(flag)

        if ischar(flag) && strcmp(flag, 'init')
            %
            % TODO
            %

        elseif ischar(flag) && strcmp(flag, 'done')
            %
            % TODO
            %

        end

    else
        %
        % TODO
        %
        
    end

    %
    % TODO
    %
    status = 0;
    
end
