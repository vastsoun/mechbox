%
%   function [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeDep(L, Q, C, conf)
%
%   Generates Equations of Motion with time-dependent state vectors, i,e, q
%   = q(t).
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeDep(L, Qin, Cin, conf)

    %%%
    %   Euler-Lagrange Equation:
    %
    %   D/Dt(dL/d(d_q)) - dL/d(q) = Q
    %
    %   Where:
    %           D/Dx : Absolute derivative wrt to x
    %           d/dx : Partial derivative wrt to x 
    %
    %%%
    
    Largs = argnames(L);
    N = numel(Largs);
    nq = (N - 1)/2;
    tin = Largs(:,1);
    qin = Largs(:,2:(1+nq));
    Dqin = Largs(:,(2+nq):(2*nq+1));
    syms DDqin real;
    syms q(t) F(t);

    for i = 1:nq
        
        index = int2str(i);

        if i == 1
            eval(['syms q_' index '(t);']); 
            eval(['q = q_' index ';']);
            eval(['syms DDq_' index ' real;']); 
            eval(['DDqin = DDq_' index ';']);
        else
            eval(['syms q_' index '(t);']); 
            eval(['q = [q; ' 'q_' index '];']);
            eval(['syms DDq_' index ' real;']); 
            eval(['DDqin = [DDqin;' 'DDq_' index '];']);   
        end

    end
    
    DDqin = DDqin.';
    Fin = argnames(Qin);
    nf = uint64(max(size(Fin, 1)));
    
    if nf > 0
        for i = 1:nf

            index = int2str(i);

            if i == 1
                eval(['syms F_' index '(t);']); 
                eval(['F = F_' index ';']);
            else
                eval(['syms F_' index '(t);']); 
                eval(['F = [F; ' 'F_' index '];']);
            end

        end
    else
        F = [];
    end
        
    % Define derivatives of generalized coordinates
    Dq = diff(q, t, 1);
    DDq = diff(q, t, 2);
    
    % Calculate individual partial and absolute derivatives.
    dL_dq   = jacobian(L, qin).';
    dL_dDq  = jacobian(L, Dqin).';
    DdL_dDq = jacobian(dL_dDq, tin) + jacobian(dL_dDq, qin)*Dqin.' + jacobian(dL_dDq, Dqin)*DDqin.';

    % Simplify exrpessions.
    dL_dq = simplify(dL_dq, 'Steps', 100);
    DdL_dDq = simplify(DdL_dDq, 'Steps', 100);

    %
    if ~isempty(Cin) && (~isempty(Cin.configuraton) || ~isempty(Cin.motion))
       
        % Get number of constraints specified
        nc_c = numel(Cin.configuraton);
        nc_m = numel(Cin.motion);
        
        % Generate a langrangian multiplier for each constraint
        for i = 1:nc_c+nc_m
        
            index = int2str(i);

            if i == 1
                eval(['syms lambda_' index '(t);']); 
                eval(['Lambda = lambda_' index ';']);
                eval(['syms lambda' index ';']); 
                eval(['Lambdain = lambda' index ';']);
            else
                eval(['syms lambda_' index '(t);']); 
                eval(['Lambda = [Lambda; ' 'lambda_' index '];']);
                eval(['syms lambda' index ';']); 
                eval(['Lambdain = [Lambdain; ' 'lambda' index '];']);
            end

        end
        
        % Initialize the generalized forces from the constraint forces
        Q_lambda_in = sym(zeros(nq,1));
        
        % Calculate reaction forces in each generalized coordinate
        % dimension over all lambdas for configuration contraints
        for i = 1:nq
            for j = 1:nc_c
                Q_lambda_in(i) = Q_lambda_in(i) + Lambdain(j)*jacobian(Cin.configuraton(j), qin(i));
            end
        end
        
        % Calculate reaction forces in each generalized coordinate
        % dimension over all lambdas for motion contraints
        for i = 1:nq
            for j = nc_c+1:nc_c+nc_m
                Q_lambda_in(i) = Q_lambda_in(i) + Lambdain(j)*jacobian(Cin.motion(j-nc_c), Dqin(i));
            end
        end
        
        % Generate the generalized forces for the reaction forces
        Q_lambda(t) = subs(Q_lambda_in, [qin Dqin DDqin Lambdain.'], [q.' Dq.' DDq.' Lambda.']);
        
    else
        Lambda = [];
        Q_lambda(t) = sym(zeros(nq,1)); 
    end
    
    % Calculate the Equations of Motion
    EoMin(tin, qin, Dqin, DDqin) = DdL_dDq - dL_dq;
    
    % Substitute static variables with time-dependent forms
    Q(t) = subs(Qin, [qin Dqin DDqin Fin], [q.' Dq.' DDq.' F]);
    EoM(t) = subs(EoMin, [qin Dqin DDqin], [q.' Dq.' DDq.']);
    
    % Generate the differential EoM
    EoM(t) = EoM(t) + Q_lambda(t) == Q(t);
    EoM(t) = simplify(EoM, 'Steps', 100);
    
    % Append Constraints to EoM to form DAE system.
    if ~isempty(Cin) && (~isempty(Cin.configuraton) || ~isempty(Cin.motion))
        if isempty(Cin.configuraton) && ~isempty(Cin.motion)
           Cm(t) = subs(Cin.motion, [qin Dqin DDqin], [q.' Dq.' DDq.']);
           EoM(t) = [EoM(t); Cm(t).' == 0];
           
        elseif ~isempty(Cin.configuraton) && isempty(Cin.motion)
            Cc(t) = subs(Cin.configuraton, [qin Dqin DDqin], [q.' Dq.' DDq.']);
            EoM(t) = [EoM(t); Cc(t).' == 0];
            
        elseif ~isempty(Cin.configuraton) && ~isempty(Cin.motion)
            Cc(t) = subs(Cin.configuraton, [qin Dqin DDqin], [q.' Dq.' DDq.']);
            Cm(t) = subs(Cin.motion, [qin Dqin DDqin], [q.' Dq.' DDq.']);
            EoM(t) = [EoM(t); Cc(t).' == 0; Cm(t).' == 0];
                        
        end 
    end
end
