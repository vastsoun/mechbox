%
%   obj = mbCreateSphere(axes,objgroup,radius,rgbcolor,transparency,position,orientation)
%
%   Generates and plots a 3D sphere with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreateSphere(axes,objgroup,radius,rgbcolor,transparency,position,orientation)

    % Create initial transfor group for pose initialization
    spheretf = hgtransform('Parent',axes);

    % Create the cylindrical shaft
    [X,Y,Z] = sphere();
    C = zeros(size(X,1),size(X,2));
    obj =  mbCreateMesh(axes,radius*X,radius*Y,radius*Z,C);
    obj.Parent = spheretf;
    obj.EdgeColor = 'none';
    obj.FaceColor = rgbcolor;
    obj.FaceAlpha = transparency;
    obj.FaceLighting = 'gouraud';

    % Initialzie to desired pose
    Txyz = makehgtform('translate', position);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(spheretf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    obj.Parent = objgroup;

end
