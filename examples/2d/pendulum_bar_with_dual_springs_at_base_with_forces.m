%%
%   File:           pendulum_bar_with_dual_springs_at_base_with_forces.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           16/6/2016
%
%   Desciption:     Analysis of a system with a single shaft/bar as a
%                   pendulum and which is suspended from a horizontally
%                   mving base which slides along a linear guide, and is
%                   supported by a spring on either side.
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [-g 0 0].';

%% Define Bodies

% Define massess
syms m_1 m_2 real;

% Define rotational inertias wrt to CoM
syms I_1 I_2 real;

% Model parameters
syms k L mu real;

% Moment of inertia wrt to CoM
I_1 = [0 0 0; 0 0 0; 0 0 0].';
I_2 = [0 0 0; 0 0 0; 0 0 (1/12)*m_2*L^2].';

%% Define Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame.
%   {P_n,i_n,j_n,k_n} - Local frame at point P_n. 
%%%

% Define absolute coodrinates
syms x_1 y_1 theta_1 real; % CoM of slider m_1
syms x_2 y_2 theta_2 real; % CoM of slider m_2

% Define absolute velocities
syms Dx_1 Dy_1 Dtheta_1 real; 
syms Dx_2 Dy_2 Dtheta_2 real; 

% Define absolute accelerations
syms DDx_1 DDy_1 DDtheta_1 real; 
syms DDx_2 DDy_2 DDtheta_2 real; 

%% Absolute Coordinates

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_1 y_1 theta_1 x_2 y_2 theta_2].';
Du = [Dx_1 Dy_1 Dtheta_1 Dx_2 Dy_2 Dtheta_2].';
DDu = [DDx_1 DDy_1 DDtheta_1 DDx_2 DDy_2 DDtheta_2].';

%% System Constraints

% Holonomic Constraints
f_1 = x_1;
f_2 = theta_1;
f_3 = x_2 - (L/2)*cos(theta_2);
f_4 = y_2 - (L/2)*sin(theta_2) - y_1;

% Velocity form of holonomic-constraints in absolute coordinates
Df_1 = Dy_1;
Df_2 = Dtheta_1;
Df_3 = Dx_2 + Dtheta_2*(L/2)*sin(theta_2);
Df_4 = Dy_2 - Dtheta_2*(L/2)*cos(theta_2) - Dy_1;

% Non-holonomic Constraints
% NONE

%% Generalized Coordinates

% Generazied quantities
syms q Dq DDq Q real;

% Define the generalized quantities
q = [y_1 y_2 theta_2].';
Dq = [Dy_1 Dy_2 Dtheta_2].';
DDq = [DDy_1 DDy_2 DDtheta_2].';

%% Forward Kinematics 

% Application of constraints to positions
theta_1 = 0;
x_1 = 0;
x_2 = (L/2)*cos(theta_2);

% Application of constraints to velocities
Dtheta_1 = 0;
Dx_1 = 0;
Dx_2 = - (L/2)*Dtheta_2*sin(theta_2);

% Absolute position vectors
r_1 = [x_1 y_1 0].';
r_2 = [x_2 y_2 0].';

% Absolute velocity vectors
v_1 = [Dx_1 Dy_1 0].';
v_2 = [Dx_2 Dy_2 0].';

% Angular velocities of CoM 
omega_1 = [0 0 Dtheta_1].';
omega_2 = [0 0 Dtheta_2].';

%% External Forces/Torques

% This system has a single external torque only
syms F_y_ext T_theta_ext real;

%
syms T F real;

% Define the exernal force in the dimensions of the absolute linear
% displacements
Targs = T_theta_ext;

% DDefine the exernal force in the dimensions of the absolute angular
% displacements
Fargs = [];

%% Generalized Forces

% Define the generalized forces from absolute forces acting at point D
syms Q_y Q_theta real;

% Define external forces in the dimension of each generalized coordinate
Q_y1 = -mu*m_1*g*sign(Dy_1); % Coulomb kinetic friction
Q_y2 = 0;
Q_theta = T_theta_ext;      % Externally applied torque

% Generalized external forces as a function of the absolute external forces
Q_sys([Fargs Targs].') = [Q_y1 Q_y2 Q_theta].';

%% Kinetics

% Kinetic energy terms
T_1 = (1/2)*m_1*(v_1.'*v_1) + (1/2)*omega_1.'*I_1*omega_1;
T_2 = (1/2)*m_2*(v_2.'*v_2) + (1/2)*omega_2.'*I_2*omega_2;

T_1 = simplify(T_1, 'Steps', 100);
T_2 = simplify(T_2, 'Steps', 100);

% Total kinetic energy 
T_sys = T_1 + T_2;
T_sys = simplify(T_sys, 'Steps', 100);

% Potential energy terms
V_k1 = (1/2)*k*(y_1)^2;
V_k2 = (1/2)*k*(y_1)^2;
V_g1 = m_1*r_1.'*e_g;
V_g2 = m_2*r_2.'*e_g;

V_k1 = simplify(V_k1, 'Steps', 100);
V_k2 = simplify(V_k2, 'Steps', 100);
V_g1 = simplify(V_g1, 'Steps', 100);
V_g2 = simplify(V_g2, 'Steps', 100);

% Total potential energy 
V_sys = V_k1 + V_k2 + V_g1 + V_g2;
V_sys = simplify(V_sys, 'Steps', 100);

%% System Lagrangian

% System Lagrangian[q, Dq, DDq]
L_sys(t, q.', Dq.') = T_sys - V_sys;
L_sys = simplify(L_sys, 'Steps', 100);

% Display resulting Lagrangian function
display('System Lagrangian: ');
pretty(L_sys == 0);

%% Augmentation Constraints

% Define the constraints
C_sys.configuraton = f_4;
C_sys.motion = [];

%% Calculate Constrained EoM

% Generator configurations
eulagconf.Type = 'TimeDependent';

% Compute EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

% Display resulting EoM
display('Equations of Motion: ');
pretty(EoM);

%% Generate System ODE

% Set which variables represent system parametes
sysparams = [g mu k m_1 m_2 L].';

% Generator configurations
dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, Dsysvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Numerical Solutions

% Set the simulation parameters for the system
%siminput = @(t) sin(t);
siminput = @(t) 0.0;
simparams = [9.81 0.1 10.0 1.0 2.0 1.0].';
siminit = [0.0 pi/8 0.1 0.0 0.0 0.0 0.0 0.0 0.0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 10.0;
simconf.timeStep = 1e-5;
simconf.solverType = 'ode15i';
simconf.notification = true;

% Run the Simulation
[simdata, simtime, solverTime] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Define the Simulation Environment

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_1 = @(input,params) [0; input(1); 0.0];
tf_2 = @(input,params) [params(6)*cos(input(3)); input(2) + params(6)*sin(input(3)); 0.0];

bodies(1).tfs = tf_0;
bodies(1).shapePrimitiveType = 'line';
bodies(1).shapePrimitiveParams = [simparams(6)*2 2];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).name = '0';
bodies(1).showCS = true;
bodies(1).showTrail = true;

%% Run the Simulation Visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = [-0.2 1.8; -1 1; -1 1];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.15;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 20;

% Run the visualization
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% SimulationOverview

simovconf.FontSize = 22;
simovconf.DataSet = [];
simovconf.Print = [];

mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
