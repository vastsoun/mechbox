%
%   simviz = mbSimulationVisualizer(bodies, params, data, time, conf)
%
%   TODO
%
%   Inputs:
%       bodies          An array of body structs which encapsulates the
%                       geometric primtives and kinematic transformations
%                       required for visualizing a body.
%       params          Values for the system parameters to be used in the
%                       simulation.
%       data            Input data produced from the simulation.
%       time            Vector of time stemps across simulation.
%
%
%       conf.viewdim    Visualization mode for 2D or 3D simulations.
%       conf.fps        Desired Frames-Per-Second for the visualization.
%       conf.recordmode Flag to disable/enable recoding video of
%                       simulation.
%       conf.limitrate  Flag to enable/disable visualization limit rate for
%                       video aliasing.
%       conf.camlight   Disable(false) or enable(true) camlight in plot.
%       conf.recordmode Disable(false) or enable(true) recoring AVI video
%                       of the visualization.
%       conf.csscale    Configure the scale of the CS frames.
%       conf.csfontsize CS frame font size.
%       conf.debugmode  Debug mode moves frame-by-frame with every click or
%                       keypress from the user.
%       conf.WaitPrompt Disable the automatic execution of the simulation
%                       and waits for user's input. 
%       conf.FontSize   Global fontsize. All titles, labels and axes ticks
%                       are scaled proportionately. 
%       conf.Print      Set to 'pdf' if an export of the figure at the end
%                       of the simulation is desired. 
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function mbSimulationVisualizer(bodies, params, data, time, conf)

    % Console output
    display('MechBox::SimulationVisualizer: ');
    display('    Generating visualization environment...');

    % Internal constants
    trail_colors = {'red','blue','green','cyan','magenta','yellow','red','blue','green','cyan','magenta','yellow','red','blue','green','cyan','magenta','yellow'};
    %simzoom = 0.5;
    
    % Configure the visualation parameters
    n = uint64(size(time,2));
    Np = numel(bodies);
    
    if ~isempty(conf) && isfield(conf, 'fps') && ~isempty(conf.fps)
        if conf.fps > 0
            time_visualization = 1.0/conf.fps;
            time_duration = time(n);
            time_steprate = time(2) - time(1);
            time_scale = uint64(time_visualization/time_steprate);
            Nt = uint64(time_duration/time_steprate);
            Nf = Nt/time_scale;
        elseif numel(time) > 1
            Nf = numel(time);
            time_visualization = 1;
            time_scale = 1;
        else
            Nf = 2;
            time_visualization = 1;
            time_scale = 1;
            data = [data data];
            time = [0 1];
        end
    else
        error('mbSimulationVisualizer: Invalid value specified for simiz "fps"');
    end
    
    % Downsample the data-set for smooth visualization
    X_sim_movie = downsample(data.',time_scale);
    T_sim_movie = downsample(time.',time_scale);
    X_sim_movie = X_sim_movie.';
    T_sim_movie = T_sim_movie.';
    Nq = size(X_sim_movie,1)/2;

    if ~isempty(conf.FontSize)
        fontsize = conf.FontSize;
    else
        fontsize = 18;
    end
    
    % Create visualiaztionfigure
    simviz_figure = figure('units','normalized','outerposition',[0 0 1 1]);
    simviz_figure.NumberTitle = 'off';
    simviz_figure.Name = 'MechBox : 3D Task-Space Visualization';

    % Generate the primary 3D simulation window
    simviz_3d_axes = axes(simviz_figure);
    subplot(3,4,[1:3 5:7 9:11], simviz_3d_axes);
    set(simviz_3d_axes,'TickLabelInterpreter', 'latex','FontSize',fontsize-4);
    
    %
    if isfield(conf, 'viewrange') && ~isempty(conf.viewrange) && isa(conf.viewrange, 'numeric')
        if isfield(conf, 'viewdim') && ~isempty(conf.viewdim)
           switch conf.viewdim
               case '3D'
                   axis(simviz_3d_axes, 'equal'); 
                   axis(simviz_3d_axes, [conf.viewrange(1,1) conf.viewrange(1,2) conf.viewrange(2,1) conf.viewrange(2,2) conf.viewrange(3,1) conf.viewrange(3,2)]);
                
               case '2D'
                   axis(simviz_3d_axes, 'equal'); 
                   axis(simviz_3d_axes, [conf.viewrange(1,1) conf.viewrange(1,2) conf.viewrange(2,1) conf.viewrange(2,2)]);
                   
               otherwise
                    error('error: mbSimulationVisualizer: Incorrect configuration of world perspective');
           end
        end
    else
        axis(simviz_3d_axes, 'equal');
    end
     
    %
    grid on;
    
    if ~isempty(conf) && isfield(conf, 'camlight') && conf.camlight == true
        camlight;
    end
    title('3D Cartesian Space','Interpreter','latex','FontSize',fontsize-2)
    xlabel('x [m]','Interpreter','latex','FontSize',fontsize-2);
    ylabel('y [m]','Interpreter','latex','FontSize',fontsize-2);
    
    if isfield(conf, 'viewdim') && ~isempty(conf.viewdim)
       switch conf.viewdim
           case '3D'
               zlabel('z [m]','Interpreter','latex','FontSize',fontsize-2);
               view(3);
               axis(simviz_3d_axes,'vis3d');
           case '2D'
               view(2);
               axis(simviz_3d_axes,'vis3d');
           otherwise
                error('error: mbSimulationVisualizer: Incorrect configuration of world perspective');
       end
    end
    
    % Create the body transform groups
    for j = 1 : Np+1
        bodytf{j,1} = hgtransform('Parent',simviz_3d_axes);
        bodytf{j,2} = hgtransform('Parent',simviz_3d_axes);
    end
    
    % Configure the zoom callback for font-scaling
    simviz_3d_zoom = zoom; % get handle to zoom utility
    set(simviz_3d_zoom,'ActionPostCallback',@mbSimVizZoomCallback);
    set(simviz_3d_zoom,'Enable','on');
    
    %
    if ~isfield(conf, 'csscale') || isempty(conf.csscale)
        csscale = 0.2;
    else
        csscale = conf.csscale;
    end
    
    %
     if ~isfield(conf, 'csfontsize') || isempty(conf.csfontsize)
        csfontsize = 11;
    else
        csfontsize = conf.csfontsize;
    end
    
    % Create the global inertial frame
    [csframes{Np+1}, csflabels{Np+1}] = mbCreateVisualCoordinateFrame(simviz_3d_axes,bodytf{Np+1,1},'I',[0 0 0],[0,0,0],csscale,csfontsize);
    csfLabelsActive{Np+1} = true;
   
    % Initialize specified points
    for j = 1 : Np
        
        if conf.debugmode == true
            waitforbuttonpress;
        end
        
        % Initialize the body-fixed coordinate-system frames per body
        if bodies(j).showCS == true
            [csframes{j}, csflabels{j}] = mbCreateVisualCoordinateFrame(simviz_3d_axes,bodytf{j,1},bodies(j).name,[0;0;0],[0;0;0],csscale,csfontsize);
            csfLabelsActive{j} = true;
        else
            csframes{j} = [];
            csflabels{j} = [];
            csfLabelsActive{j} = false;
        end
        
        if bodies(j).showTrail == true
           trails{j} = animatedline(simviz_3d_axes,'Color', trail_colors{j});
        else
            trails{j} = [];
        end
            
        % Create Bodies based on input
        if isfield(bodies(j), 'shapePrimitiveType')
            if ~isempty(bodies(j).shapePrimitiveType)
                switch bodies(j).shapePrimitiveType
                    case 'none'
                        usecsfonly{j} = 1;
                    case 'line'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateLine(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams(1),bodies(j).shapePrimitiveParams(2),bodies(j).shapeColor(2:4),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [0 0 0].';
                    case 'plane'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateSurfacePlane(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams,bodies(j).shapeColor(2:4),bodies(j).shapeColor(1),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [-bodies(j).shapePrimitiveParams(1)/2 -bodies(j).shapePrimitiveParams(2)/2 0].';
                    case 'parallelogram'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateParallelogram(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams,bodies(j).shapeColor(2:4),bodies(j).shapeColor(1),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [bodies(j).shapePrimitiveParams(1)/2 bodies(j).shapePrimitiveParams(2)/2 bodies(j).shapePrimitiveParams(3)/2].';
                    case 'cylinder'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateCylinder(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams(1),bodies(j).shapePrimitiveParams(2),bodies(j).shapeColor(2:4),bodies(j).shapeColor(1),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [0 0 bodies(j).shapePrimitiveParams(2)/2].';
                    case 'sphere'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateSphere(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams(1),bodies(j).shapeColor(2:4),bodies(j).shapeColor(1),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [0 0 0].';
                    case 'spring'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreateSpring(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams(1),bodies(j).shapePrimitiveParams(2),bodies(j).shapePrimitiveParams(3),bodies(j).shapePrimitiveParams(4),bodies(j).shapeColor(2:4),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = [0 0 0].';
                    case 'polygon'
                        usecsfonly{j} = 0;
                        objhandle{j} = mbCreatePolygon(simviz_3d_axes,bodytf{j,2},bodies(j).shapePrimitiveParams,bodies(j).shapeColor(2:4),bodies(j).shapeColor(1),[0;0;0],[0;0;0]);
                        objpositionoffsets{j} = bodies(j).shapePrimitiveParams.Offsets;
                    otherwise
                      error('mbSimulationVisualizer: Incorrect type specified for body point.');
                end
            end
        end
        
        % Apply pose offsets
        if isfield(bodies(j), 'shapeOffsets') && ~isempty(bodies(j).shapeOffsets)
            objposeoffsets{j} = bodies(j).shapeOffsets;
        else
            objposeoffsets{j} = [0 0 0 0 0 0].';
        end
        
        % Initial transformations
        init_pose = bodies(j).tfs(X_sim_movie(:,1).',params);
        
        T_xyz_csf = makehgtform('translate', init_pose(1:3));
        %Rx = makehgtform('xrotate',init_pose(4));
        %Ry = makehgtform('yrotate',init_pose(5));
        %Rz = makehgtform('zrotate',init_pose(6));
        Rot = makehgtform('axisrotate',[init_pose(4),init_pose(5),init_pose(6)],init_pose(7));
        
        if strcmp(bodies(j).shapePrimitiveType,'spring')
            scale = 1 - init_pose(7)/bodies(j).shapePrimitiveParams(1);
            Sxyz = [1 0 0 0; 0 1/scale 0 0; 0 0 1/scale 0; 0 0 0 1]*makehgtform('scale', scale);
        else
            Sxyz = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
        end
        
        set(bodytf{j,1},'Matrix', T_xyz_csf*Rot);
        if usecsfonly{j} == 0
            T_xyz_obj = makehgtform('translate', init_pose(1:3));
            Toff = makehgtform('translate', -objpositionoffsets{j} + objposeoffsets{j}(1:3));
            Rxoff = makehgtform('xrotate',objposeoffsets{j}(4));
            Ryoff = makehgtform('yrotate',objposeoffsets{j}(5));
            Rzoff = makehgtform('zrotate',objposeoffsets{j}(6));
            %set(bodytf{j,2},'Matrix', T_xyz_obj*Rx*Ry*Rz*Sxyz*Rxoff*Ryoff*Rzoff*Toff);
            Roff = Rxoff*Ryoff*Rzoff;
            set(bodytf{j,2},'Matrix', T_xyz_obj*Rot*Sxyz*Roff*Toff);
        end
        drawnow;
    end
  
    % Re-initialize the csf labels now that all frames have been placed
    simviz_axis_limits = axis(simviz_3d_axes);
    for j = 1 : Np+1
        if csfLabelsActive{j} == true
            set(csflabels{j},'FontSize',100*csscale/(simviz_axis_limits(4)-simviz_axis_limits(3)));
        end
    end
    
    % Generate the system coordinates
    simviz_states_axes = axes(simviz_figure);
    subplot(3,4,4, simviz_states_axes);
    set(simviz_states_axes,'TickLabelInterpreter', 'latex','FontSize',fontsize-4);
    grid on;
    title('System Response - ${\bf x}_{s}(t)$','Interpreter','latex','FontSize',fontsize-2)
    xlabel('$t$ [s]','Interpreter','latex','FontSize',fontsize-2);
    ylabel('${\bf x}_{s}$ [units]','Interpreter','latex','FontSize',fontsize-2);
    for j = 1 : Nq
        states_line{j} = animatedline(simviz_states_axes,'Color', trail_colors{j});
    end
   
    % Generate the system derivatives plot
    simviz_derivatives_axes = axes(simviz_figure);
    subplot(3,4,8, simviz_derivatives_axes);
    set(simviz_derivatives_axes,'TickLabelInterpreter', 'latex','FontSize',fontsize-4);
    grid on;
    title('System Derivatives - ${\frac{d{\bf x_{s}}}{dt}}(t)$','Interpreter','latex','FontSize',fontsize-2)
    xlabel('$t$ [s]','Interpreter','latex','FontSize',fontsize-2);
    ylabel('${\frac{d{\bf x_{s}}}{dt}}(t)$ [units/s]','Interpreter','latex','FontSize',fontsize-2);
    for j = 1 : Nq
        Dstates_line{j} = animatedline(simviz_derivatives_axes,'Color', trail_colors{j});
    end
    
    % Generate the dynamical system phase-space plot
    simviz_phasespace_axes = axes(simviz_figure);
    subplot(3,4,12, simviz_phasespace_axes);
    set(simviz_phasespace_axes,'TickLabelInterpreter', 'latex','FontSize',fontsize-4);
    grid on;
    title('Phase Portrait - ${\bf x}_{s}(t)$ vs ${\frac{d{\bf x_{s}}}{dt}}(t)$','Interpreter','latex','FontSize',fontsize-2)
    xlabel('${\bf x}_{s}$ [units]','Interpreter','latex','FontSize',fontsize-2);
    ylabel('${\frac{d{\bf x_{s}}}{dt}}(t)$ [units/s]','Interpreter','latex','FontSize',fontsize-2);
    for j = 1 : Nq
        phaseplot_line{j} = animatedline(simviz_phasespace_axes,'Color', trail_colors{j});
    end
    
    % Propmpt for initiation of visualization
    if ~isempty(conf) && isfield(conf, 'WaitPrompt') && conf.WaitPrompt == true
        display('    Press any key to commence execution...');
        waitforbuttonpress;
    end
    
    % Get the current limits of the axes
    initaxeslim = axis(simviz_3d_axes);
    
    % Initial data
    pose_next = zeros(6,1);
    
    % Run the visualization    
    for i = 1 : Nf-1
        % Iterate over tracked points
        for j = 1 : Np

            % Get next state data for each body
            %pose_previous = pose_next;
            pose_next = bodies(j).tfs(X_sim_movie(:,i+1).',params);

            % Generate the 3D Cartesian plot data
            if bodies(j).showTrail == true
                addpoints(trails{j},pose_next(1),pose_next(2),pose_next(3));
            end

            % Transformations
            T_xyz_csf = makehgtform('translate', pose_next(1:3));
            %Rx = makehgtform('xrotate',pose_next(4));
            %Ry = makehgtform('yrotate',pose_next(5));
            %Rz = makehgtform('zrotate',pose_next(6));
            Rot = makehgtform('axisrotate',[pose_next(4),pose_next(5),pose_next(6)],pose_next(7));
            
            if strcmp(bodies(j).shapePrimitiveType,'spring')
                scale = 1 - pose_next(8)/bodies(j).shapePrimitiveParams(1);
                Sxyz = [1 0 0 0; 0 1/scale 0 0; 0 0 1/scale 0; 0 0 0 1]*makehgtform('scale', scale);
            else
                Sxyz = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
            end

            %set(bodytf{j,1},'Matrix', T_xyz_csf*Rx*Ry*Rz);
            set(bodytf{j,1},'Matrix', T_xyz_csf*Rot);
            if usecsfonly{j} == 0
                T_xyz_obj = makehgtform('translate', pose_next(1:3));
                Toff = makehgtform('translate', -objpositionoffsets{j} + objposeoffsets{j}(1:3));
                Rxoff = makehgtform('xrotate',objposeoffsets{j}(4));
                Ryoff = makehgtform('yrotate',objposeoffsets{j}(5));
                Rzoff = makehgtform('zrotate',objposeoffsets{j}(6));
                %set(bodytf{j,2},'Matrix', T_xyz_obj*Rx*Ry*Rz*Sxyz*Rxoff*Ryoff*Rzoff*Toff);
                Roff = Rxoff*Ryoff*Rzoff;
                set(bodytf{j,1},'Matrix', T_xyz_csf*Rot);
                set(bodytf{j,2},'Matrix', T_xyz_obj*Rot*Sxyz*Roff*Toff);
            end

            % Adapt font size
            axislim = axis(simviz_3d_axes);
            for k = 1 : Np+1
                if csfLabelsActive{k} == true
                    set(csflabels{k},'FontSize',100*csscale/(axislim(4)-axislim(3)));
                end
            end
        end

        for j = 1 : Nq
            % Generate 2D response plots
            addpoints(states_line{j}, T_sim_movie(i), X_sim_movie(j,i+1));
            addpoints(Dstates_line{j}, T_sim_movie(i), X_sim_movie(j+Nq,i+1));
            addpoints(phaseplot_line{j}, X_sim_movie(j,i+1), X_sim_movie(j+Nq,i+1));
        end

        if conf.recordmode == true
            drawnow;
            if conf.fps == 0
                for j = 1:10
                    simviz((i-1)*10 + j) = getframe(simviz_figure);  
                end
            else
                simviz(i) = getframe(simviz_figure);   
            end

        else
            pause(time_visualization);
            if conf.limitrate == true
                drawnow limitrate;
            else
                drawnow;
            end

            if conf.debugmode == true
                waitforbuttonpress;
            end
        end

    end
    drawnow;
    
    % Store movie to video file
    if conf.recordmode == true
        filedatetime = datestr(now,'HH_MM_mm_dd_yy');
        simvideofile = strcat('simviz_',filedatetime,'.avi');
        simvideo = VideoWriter(simvideofile, 'Motion JPEG AVI');
        simvideo.FrameRate = 30;  
        simvideo.Quality = 90;
        open(simvideo);
        writeVideo(simvideo, simviz);
        close(simvideo);
    else
        simviz = [];
    end
    
    % Export image
    if isfield(conf, 'Print') && ~isempty(conf.Print)
       switch  conf.Print
           case 'pdf'
                set(simviz_figure,'Units','Inches');
                figdimensions = get(simviz_figure,'Position');
                set(simviz_figure,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[figdimensions(3), figdimensions(4)]);
                filedatetime = datestr(now,'HH_MM_SS_mm_dd_yy');
                print(simviz_figure,filedatetime,'-dpdf','-r0');
           otherwise
               error('Error: mbSimulationVisualizer: Incorrect export type');
       end
        
    end
    
    % Define a local zoom callback function to handle in-plot text scaling
    function mbSimVizZoomCallback(~, evd)
        
        % Since i expect to zoom in ax(4)-ax(3) gets smaller, so fontsize gets bigger.
        
        if evd.Axes == simviz_3d_axes
            
            axis_limits = axis(evd.Axes);
        
            % Adapt font size
            for k = 1 : Np+1
                if csfLabelsActive{k} == true
                    set(csflabels{k},'FontSize',100*csscale/(axis_limits(4)-axis_limits(3)));
                end
            end
        end

    end

end


