%
%   obj = mbCreateSpring(axes,objgroup,length,radius,windings,width,rgbcolor,position,orientation)
%
%   Generates and plots a 3D spring with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreateSpring(axes,objgroup,length,radius,windings,width,rgbcolor,position,orientation)
    
    % Create initial transfor group for pose initialization
    springtf = hgtransform('Parent',axes);

    % Initialize spring data
    X = linspace(-length/2,length/2,200);
    Y = zeros(1,size(X,2));
    Z = zeros(1,size(X,2));
    
    % Calculate points of helix
    N = size(X,2);
    for i = 1:N
       Z(i) = radius*cos(2*pi*X(i)*windings/length);
       Y(i) = radius*sin(2*pi*X(i)*windings/length);
    end
    
    % Create graphics object
    obj = animatedline(axes,X,Y,Z);
    obj.Parent = springtf;
    obj.Color = rgbcolor;
    obj.LineWidth = width;
    
    % Initialzie to desired pose
    Txyz = makehgtform('translate', position);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(springtf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    obj.Parent = objgroup;
    
end

