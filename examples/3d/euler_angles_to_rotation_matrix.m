%%
%   File:           euler_angles_to_rotation_matrix.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           21/6/2016
%
%   Desciption:     TODO
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;


%%

syms psi theta phi real;
Rxyz = mbRotationMatrix('x',phi)*mbRotationMatrix('y',theta)*mbRotationMatrix('z',psi);
mbSymbolic2Latex([],[],Rxyz,14,'plot');

syms r_11 r_12 r_13 r_21 r_22 r_23 r_31 r_32 r_33 real;
R = [r_11 r_12 r_13; r_21 r_22 r_23; r_31 r_32 r_33];
mbSymbolic2Latex([],[],R,18,'plot');

%
theta_1 = asin(r_13);
theta_2 = pi - asin(r_13);

%
psi = atan2(-r_12,r_11);

%%
% EOF
