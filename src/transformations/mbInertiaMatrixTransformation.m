%
%   function [ output_vector ] = mbInertiaMatrixTransformation (mass, input_inertia, r, R)
%
%   This function performs a transformation in the representation of a
%   moment of inertia matrix so that it is expressed to a new reference
%   frame.
%
%   inputs:
%       r               Vector of position of new point with which we express
%                       the input inertia.
%       R               Rotation matrix which aligns the current axes to
%                       the frame/axes at the new point to measure inertia. 
%       angle           Symbolic or numeric axis variable by which to
%                       rotate the input vector.
%       input_inertia   Input moment of inertia matrix.
%       mass            Mass oof body.
%
%   returns:
%       output_inertia   Rotated symbolic or numeric 3D vector.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function output_inertia = mbInertiaMatrixTransformation(mass, input_inertia, r, R)

    output_inertia = R*(input_inertia + mass*((r.'*r)*eye(3) - (r*r.')))*R.';

end
