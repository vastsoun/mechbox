%
%   function tfmatrix = mbRotationMatrix(axis, angle)
%
%   This function performs a 1D rotation of a input vector about one of the
%   XYZ axes using a 3x3 rotation matrix. The function supports both 
%   symbolic and numerical operations.
%
%   inputs:
%       axis            Relative axis/axes to implement the rotation.
%       angle           Symbolic or numeric axis/axes variable(s).
%
%   returns:
%       tfmatrix        Rotattion cosine matrix.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function tfmatrix = mbRotationMatrix(axis, angles)

    % Check type validity of input argument
    assert(isa(axis,'char'),'Error: "axis" argument is not a "char"');

    % Determine the type of rotation to apply
    if      axis == 'x'
        tfmatrix = RotX(angles);

    elseif  axis == 'y'
        tfmatrix = RotY(angles);

    elseif  axis == 'z'
        tfmatrix = RotZ(angles);
        
    elseif  strcmp(axis, 'xy')
        tfmatrix = RotX(angles(1))*RotY(angles(2));
        
    elseif  strcmp(axis, 'yz')
        tfmatrix = RotY(angles(1))*RotZ(angles(2));
        
    elseif  strcmp(axis, 'zx')
        tfmatrix = RotZ(angles(1))*RotX(angles(2));
    
    elseif  strcmp(axis, 'xyz')
        tfmatrix = RotX(angles(1))*RotY(angles(2))*RotZ(angles(3));
        
    elseif  strcmp(axis, 'zyz')
        tfmatrix = RotZ(angles(1))*RotY(angles(2))*RotZ(angles(3));

    else
        error('Incorrect argument value for `axis` input.')
    end

end

%% Support Functions

function R = RotX(angle)

    R = [1 0 0;
         0 cos(angle) -sin(angle);
         0 sin(angle) cos(angle)];
     
end

function R = RotY(angle)

    R = [cos(angle) 0 sin(angle);
         0 1 0;
         -sin(angle) 0 cos(angle)];
     
end

function R = RotZ(angle)

    R = [cos(angle) -sin(angle) 0;
         sin(angle) cos(angle)  0;
         0 0 1];
     
end

%% EOF
