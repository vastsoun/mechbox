%
%   function tfmatrix = mbHomogeneousTransformationMatrix (convention, rotation, translation)
%
%   TODO
%
%   inputs:
%       rotconv         Rotation parametrization convention to apply.
%       rotation        Parametrized rotation object - quaternion,
%                       euler-angles or rotation matrix.
%       translation     3D vector for linear translation.
%
%   returns:
%       tfmatrix        Output transformation matrix;
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function tfmatrix  = mbHomogeneousTransformationMatrix(rotconv, rotation, translation)

    %
    % TODO
    %
    
end

%% EOF
