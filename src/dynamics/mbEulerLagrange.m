%
%   [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrange(t, q, Dq, DDq, L, Qin, Cin, conf)
%
%   This function calculates the Equations of Motion (EoM) from a specified
%   Lagrangian function using the Euler-Lagrange equation.
%
%   Inputs:
%       L           A Lagrangian function, encoding the dynamics of a given
%                   system.
%       Q           Vector of generalized forces.
%       C           Vector of constraints for which we would like to calculate
%                   the reaction forces.
%       conf.Type   Type of system to generate; either TimeDependent ot
%                   TimeIndependent state vectors etc.
%   Returns:
%       EoM    Symbolic expression of the equations of motion.
%       q       Vector of generalized coordinates.
%       Dq      Vector of generalied velocities.
%       DDq     Vector of generalized accelerations.
%       Q       Vector of generalized external forces.
%       F       Array of force-valued variables which are the system's
%               excitations.
%       Lambda  Vector of lagrangian multipliers for constrained systems.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrange(t, q, Dq, DDq, L, Qin, Cin, conf)
    
    % Console output
    display(' ');
    display('MechBox::Euler-Lagrange ');
    display('    Generating equations of motion...');
    disp(  ['    EoM are of type: ' conf.Type]);

    %
    if ~isempty(conf) && isfield(conf, 'Type')
        switch conf.Type
            case 'TimeDependent'
                [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeDep(L, Qin, Cin, conf);
            case 'TimeIndependent'
                [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeIndep(t, q, Dq, DDq, L, Qin, Cin, conf);
            otherwise
                error('error: mbEulerLagrange: Incorrect "outputmode" has been specified.');
        end
    end
    
    % Console output
    display('    Done.');
    
end
