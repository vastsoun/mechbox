%
%   [ u , theta ] = mbRotationMatrixToAxisAngle( R )
%
%   'mbRotationMatrixToAxisAngle' performs a transformation of a rotation
%   matrix into an Angle-Axis parametrization of rotation.
%
%   Inputs:
%       R       3x3 Rotation or direct cosine matrix.
%
%   Returns:
%       u       Normalized axis of rotation vector.
%       theta   Angle of rotation.
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           10/8/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [ u , theta ] = mbRotationMatrixToAxisAngle( R )

    % Compute the angle of rotation
    theta = acos((trace(R) - 1)/2);

    % Compute the normalized axis of rotation vector
    u = (1/(2*sin(theta))) * [R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)];

end
