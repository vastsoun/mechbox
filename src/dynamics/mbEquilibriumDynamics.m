%
%   [equ, J, xeq, Dxeq, xeq0] = mbEquilibriumDynamics(t, q, Dq, DDq, L, Q, C, params, q0, conf)
%
%   Generates equilibrium dynamics for a system. This means it also
%   replaces all generalized velocities and accelerations with zero.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           4/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [equ, J, xeq, Dxeq, xeq0] = mbEquilibriumDynamics(t, q, Dq, DDq, L, Q, C, params, q0, conf)

    % Console output
    display(' ');
    display('MechBox::Equilibrium Dynamics ');
    display('    Generating equations of motion at equilibrium...');
    disp(  ['    EoM are of type: ' conf.Type]);

    % Dimensions of the system
    Nq = numel(q);

    % Check if any states have been inhibited/fixed
    if ~isempty(conf) && isfield(conf, 'FixedStates')
       
        for i = 1:numel(conf.FixedStates)
            if conf.FixedStates(i) == 0
                error('Error: mbEquilibriumDynamics: Invalid index specified for fixed subset of states.');
            end
        end
        
        display('    The following states have been fixed:');
        display(' ');
        disp(conf.FixedStates);
        
        % Set the free DoFs which will avoid singularities of J_eq
        alldofs = q;
        alldofsindeces = 1:Nq;
        fixeddofs = conf.FixedStates;
        [freedofs, freedofindeces] = setdiff(alldofs,fixeddofs,'stable');
        fixeddofindeces = setdiff(alldofsindeces,freedofindeces,'stable');
        
        xeq = freedofs;
        Dxeq = Dq(freedofindeces);
        xeq0 = q0(freedofindeces);
        Dx = Dq(freedofindeces);
        DDx = DDq(freedofindeces);
        L_eq = subs(L, q(fixeddofindeces), q0(fixeddofindeces));
        if ~isempty(Q)
            Q_eq = Q(freedofindeces);
            Q_eq = subs(Q_eq, q(fixeddofindeces), q0(fixeddofindeces));
        else
            Q_eq = [];
        end
        if ~isempty(C)
            C_eq = C(freedofindeces);
            C_eq = subs(C_eq, q(fixeddofindeces), q0(fixeddofindeces));
        else
            C_eq = [];
        end
        
    else
        xeq = q;
        xeq0 = q0;
        Dx = Dq;
        DDx = DDq;
        L_eq = L;
        Q_eq = Q;
        C_eq = C;
    end
    
    Nx = numel(xeq);
    
    % At equilibirum, all motion is null
    Dx_eq = zeros(Nx,1);
    DDx_eq = zeros(Nx,1);

    % Generate symbolic expressions for the equations of motion as a vector
    % valued implicit function
    eulagconf.Output = '';
    eulagconf.Type = conf.Type;
    equ.symb = mbEulerLagrange(t, xeq, Dx, DDx, L_eq, Q_eq, C_eq, eulagconf);
    
    % Evaluate EoM at equilibrium by inhibiting motion
    equ.symb = subs(equ.symb, [Dx DDx], [Dx_eq DDx_eq]);

    % Generate symbolic expression of the Jacobian of the equilibrium
    % dynamics
    J.symb = jacobian(equ.symb, xeq).';
    
    % Generate numerical implementations for the symbolic 
    if ~isempty(conf) && isfield(conf, 'Output') && strcmp(conf.Output,'Numerical')
        equ.numr = matlabFunction(equ.symb, 'vars', {xeq, params});
        J.numr = matlabFunction(J.symb, 'vars', {xeq, params});
    end
    
    % Console output
    display('    Done.');

end
