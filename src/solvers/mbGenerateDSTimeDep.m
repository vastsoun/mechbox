%
%   function [sysf, vars, Dvars] = mbGenerateDSTimeDep(equ, params, t, q, Dq, DDq, F, Lambda, conf)
%
%   This function generates a dynamical system representation from a set of
%   EoM, which can then be solved using numerical solvers.
%
%   inputs:
%       equ     Symbolic expression of the system equations
%       params  Vector of symbolic variables representing system parameters
%       t       Symbolic variable for time
%       q       Symbolic vector of generalized coordinates
%       F       Vector of parametrized external forces
%       Lambda  Vector of Lagrangian multipliers for constrained systems
%
%   returns:
%       sysf    Function handle to the system dynamics generator
%       vars    Vector of symbolic state-variables for the generated system
%       Dvars   Vector of state variables expressed as functions of 
%               derivatives of other state variables.
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           1/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [sysf, vars, Dvars] = mbGenerateDSTimeDep(equ, params, t, q, F, Lambda, conf)

    % Check if we are solving constrained EoM
    if isempty(Lambda)
        
        % Reduce system to first order ODE system and return implicit
        % expression
        [syseq, vars, Dvars] = reduceDifferentialOrder(equ, q);

        %
        [m,f] = massMatrixForm(syseq, vars);

        %
        odesys = m\f;
        odevars = vars;

        %
        if ~isempty(conf)
            if isfield(conf, 'createFile')
                if conf.createFile == true
                    if ~isempty(F)
                        sysf = odeFunction(odesys, odevars, F(t), params, 'File', 'dynamicsfunc');
                    else
                        sysf = odeFunction(odesys, odevars, params, 'File', 'dynamicsfunc');
                    end
                else
                    if ~isempty(F)
                        sysf = odeFunction(odesys, odevars, F(t), params);
                    else
                        sysf = odeFunction(odesys, odevars, params);
                    end
                    
                end
            end 
        else
            sysf = odeFunction(odesys, odevars, params, F(t));
        end
    
    % If lambdas have been specified, then the EoM are uncconstrained
    else
        
        %
        [syseq, vars] = reduceDifferentialOrder(equ, [q.' Lambda.']);

        if ~isLowIndexDAE(syseq, vars)
        
            [daesys,daevars, ~] = reduceDAEIndex(syseq,vars);
            [daesys,daevars, Ddaevars] = reduceRedundancies(daesys,daevars);
            
            if ~isLowIndexDAE(daesys, daevars)
                error('mbGenerateODE: failed to reduce DAE order.');
            end
            
        end
        
        %
        if ~isempty(conf)
            if isfield(conf, 'createFile')
                if conf.createFile == true
                    sysf = daeFunction(daesys, daevars, F(t), params, 'File', 'dsfunc');
                else
                    sysf = daeFunction(daesys, daevars, F(t), params);
                end
            end 
        else
            sysf = daeFunction(daesys, daevars, F(t), params);
        end
        
        vars = daevars;
        Dvars = Ddaevars;
        
    end

end
