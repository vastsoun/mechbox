%
%   function expr = mbRunFreeDS( extcallback, odefunc, t, ARGS, params)
%
%   ODE solver callback function for unactuated systems.
%
%   inputs:
%       extcallback     Function handle to an external callback operation 
%                       which is to be executed at every call to the custom
%                       'odefunc()' system dynamics ODE function. 
%       odefunc         The handle to the function which implements the 
%                       dynanimcs of an ODE/DAE system.  
%       t               The symbolic time variable.
%       ARGS            The array of arguments to be passed to the solver.
%       params          The array of parameter values used by the solver.
%       inputs          Vector-valued function (array) of the specified
%                       external inputs the ODE/DAE.
%
%   returns:
%       expr            Symbolic expression to be interpreted numerically
%                       by the solver to calculate the system dynamics at 
%                       each iteration.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function expr = mbRunFreeDS(extcallback,dsfunc,t,states,auxstates,params)

    % Determine if system is of type ODE or DAE
    if isempty(auxstates)
        % Execute the ODE dynamics implementation
        expr = dsfunc(t,states,params);
    else
        % Execute the DAE dynamics implementation
        expr = dsfunc(t,states,auxstates,params);
    end
    
    % If the callback handle is not empy, execute the callback operation.
    if ~isempty(extcallback)
        extcallback();
    end
    
end
