%%
%   File:           cart_with_homogeneous_cylinder.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           19/6/2016
%
%   Desciption:     Analysis of a system consisting of a cart, rolling in
%                   1D and attached to spring, while having a hollow 
%                   interior such that a cylinder is free to roll inside. 
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [0 g 0].';

%% Define Bodies

% Define massess
syms m_1 m_2 real;
assume(m_1>0 & m_2>0);

% Define rotational inertias wrt to CoM
syms I_1 I_2 real;

% Model parameters
syms k d R_1 R_2 L real;

% Moment of inertia wrt to CoM
I_1 = [0 0 0; 0 0 0; 0 0 0].';
I_2 = [0 0 0; 0 0 0; 0 0 (1/2)*m_2*R_2^2].';

%% Define Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame.
%   {P_n,i_n,j_n,k_n} - Local frame at point P_n. 
%%%

% Define absolute coodrinates
syms x_1 y_1 theta_1 real; % CoM of slider m_1
syms x_2 y_2 theta_2 real; % CoM of slider m_2

% Define absolute velocities
syms Dx_1 Dy_1 Dtheta_1 real; 
syms Dx_2 Dy_2 Dtheta_2 real; 

% Define absolute accelerations
syms DDx_1 DDy_1 DDtheta_1 real; 
syms DDx_2 DDy_2 DDtheta_2 real; 

%% Absolute Coordinates

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_1 y_1 theta_1 x_2 y_2 theta_2].';
Du = [Dx_1 Dy_1 Dtheta_1 Dx_2 Dy_2 Dtheta_2].';
DDu = [DDx_1 DDy_1 DDtheta_1 DDx_2 DDy_2 DDtheta_2].';

%% System Constraints

% Holonomic Constraints
f_1 = y_1 -d;
f_2 = theta_1;
f_3 = x_2 - (x_1 + R_1 + d) - (R_1 - R_2)*sin(-(R_2/R_1)*theta_2);
f_4 = y_2 - (R_1 + d) + (R_1 - R_2)*cos(-(R_2/R_1)*theta_2);

% Velocity form of holonomic-constraints in absolute coordinates
Df_1 = Dy_1;
Df_2 = Dtheta_1;
Df_3 = Dx_2 - Dx_1 + (R_1 - R_2)*cos(-(R_2/R_1)*theta_2)*(R_2/R_1)*Dtheta_2;
Df_4 = Dy_2 + (R_1 - R_2)*sin(-(R_2/R_1)*theta_2)*(R_2/R_1)*Dtheta_2;

% Non-holonomic Constraints
% NONE

%% Generalized Coordinates

% Generazied quantities
syms q Dq DDq Q real;

syms x theta real;
syms Dx Dtheta real;
syms DDx DDtheta real;

% Define the generalized quantities
q = [x theta].';
Dq = [Dx Dtheta].';
DDq = [DDx DDtheta].';

%% Forward Kinematics 

% Application of constraints to positions

x_1 = x + R_1 + d;
y_1 = d;
theta_1 = 0;

theta_2 = -(R_1/R_2)*theta;
x_2 = (x_1 + R_1 + d) + (R_1 - R_2)*sin(theta);
y_2 = (R_1 + d) - (R_1 - R_2)*cos(theta);

% Application of constraints to velocities

Dx_1 = Dx;
Dy_1 = 0;
Dtheta_1 = 0;

Dtheta_2 = -(R_1/R_2)*Dtheta;
Dx_2 = Dx_1 + (R_1 - R_2)*cos(theta)*Dtheta;
Dy_2 = (R_1 - R_2)*sin(theta)*Dtheta;

% Absolute position vectors
r_1 = [x_1 y_1 0].';
r_2 = [x_2 y_2 0].';

% Absolute velocity vectors
v_1 = [Dx_1 Dy_1 0].';
v_2 = [Dx_2 Dy_2 0].';

% Angular velocities of CoM 
omega_1 = [0 0 Dtheta_1].';
omega_2 = [0 0 Dtheta_2].';

%% External Forces/Torques

% This system has a single external torque only
syms F_x_ext T_theta_ext real;

%
syms Targs Fargs real;

% Define the exernal force in the dimensions of the absolute linear
% displacements
Targs = [];

% Define the exernal force in the dimensions of the absolute angular
% displacements
Fargs = F_x_ext;

%% Generalized Forces

% Define the generalized forces from absolute forces acting at point D
syms Q_x Q_theta real;

% Define external forces in the dimension of each generalized coordinate
Q_x = F_x_ext;
Q_theta = 0;      % Externally applied torque

% Generalized external forces as a function of the absolute external forces
Q_sys([Fargs Targs].') = [Q_x Q_theta].';

%% Kinetics

% Kinetic energy terms
T_1 = (1/2)*m_1*(v_1.'*v_1) + (1/2)*omega_1.'*I_1*omega_1;
T_2 = (1/2)*m_2*(v_2.'*v_2) + (1/2)*omega_2.'*I_2*omega_2;

T_1 = simplify(T_1, 'Steps', 100);
T_2 = simplify(T_2, 'Steps', 100);

% Total kinetic energy 
T_sys = T_1 + T_2;
T_sys = simplify(T_sys, 'Steps', 100);

% Potential energy terms
V_k = (1/2)*k*(x_1 - d - R_1 - L)^2;
V_g1 = m_1*r_1.'*e_g;
V_g2 = m_2*r_2.'*e_g;

V_k = simplify(V_k, 'Steps', 100);
V_g1 = simplify(V_g1, 'Steps', 100);
V_g2 = simplify(V_g2, 'Steps', 100);

% Total potential energy 
V_sys = V_k + V_g1 + V_g2;
V_sys = simplify(V_sys, 'Steps', 100);

%% System Lagrangian

% System Lagrangian[q, Dq, DDq]
L_sys(t, q.', Dq.') = T_sys - V_sys;
L_sys = simplify(L_sys, 'Steps', 100);

% Display resulting Lagrangian function
display('System Lagrangian: ');
pretty(L_sys == 0);

%% Augmentation Constraints

% Define the constraints
C_sys.configuraton = [];
C_sys.motion = [];

%% Calculate Constrained EoM

% Generator Configurations
eulagconf.Type = 'TimeDependent';

% Calculate EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

% Display resulting EoM
display('Equations of Motion: ');
pretty(EoM);

%% 

% Set which variables represent system parametes
sysparams = [g k d R_1 R_2 L m_1 m_2].';

% Configure the DS generator
dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, sysDvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Numerical Solutions

% Set the simulation parameters for the system
%siminput = @(t) sin(t);
siminput = @(t) 0.0;
simparams = [9.81 100.0 0.02 0.3 0.03 0.1 1.0 0.5].';
siminit = [0.2 0.0 0.0 0.0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 5.0;
simconf.timeStep = 1e-3;
simconf.solverType = 'ode45';
simconf.notification = false;

%
[simdata, simtime, solverTime] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Define forward kinematics transformations callbacks

% Set the transformation function which produces cartesian trajectories from system trajectories
tf_1 = @(input,params) [input(1) + params(4) + params(3); params(4) + params(3); 0.0; 0.0; 0.0; 0];
tf_2 = @(input,params) [(input(1) + params(4) + params(3)) + (params(4) - params(5))*sin(input(2)); (params(4) + params(3)) - (params(4) - params(5))*cos(input(2)); 0; 0; 0; (-params(4)/params(5))*input(2)];

%% Define geometric primitives of the bodies for simulation visualization

bodies(1).tfs = tf_1;
bodies(1).shapePrimitiveType = 'cylinder';
bodies(1).shapePrimitiveParams = [simparams(4) 0.1];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6];
bodies(1).name = '1';
bodies(1).showCS = true;
bodies(1).showTrail = true;

bodies(2).tfs = tf_2;
bodies(2).shapePrimitiveType = 'cylinder';
bodies(2).shapePrimitiveParams = [simparams(5) 0.1];
bodies(2).shapeColor = [0.3 0.6 0.8 0.8];
bodies(2).name = '2';
bodies(2).showCS = true;
bodies(2).showTrail = true;

%% Run the simulation visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '2D';
simvizconf.viewrange = [];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.1;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 20;

% Run the visualization service
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% Simulation Overview

simovconf.FontSize = 22;
simovconf.DataSet = [];
simovconf.Print = [];

mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
