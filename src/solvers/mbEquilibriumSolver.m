%
%   [equsol, equstate] = mbEquilibriumSolver(state, equ, J, params, initstate, conf)
%
%   This function performs symbolically a 1D rotation of a input vector
%   about one of the XYZ axes using a 3x3 rotation matrix.
%
%   Inputs:
%       state               The state vector of the system to solve.
%       equ                 The vector valued function of the system
%                           dynamics.
%       J                   Jacobian of the system dynamics.
%       params              System parameters array.
%       initstate           Initial guess to be used by solver.
%       
%       conf.Solver         Currently only supports 'NewtonRaphson'.
%       conf.MaxIterations  Maximum number of solver iterations.
%       conf.MaxError       Maximum error tolerance for euclidean norm
%                           termination condition
%       conf.FullState      The full state of the system.
%       conf.FixedStates    Fixed states which are to remain constant.
%       conf.FullInitState  Constant values for the fixed states.
%       conf.Print          Set to 'pdf' if an export of the figure at the end
%                           of the simulation is desired. 
%       
%   Returns:
%       equsol
%       equstate
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           11/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [equsol, equstate] = mbEquilibriumSolver(state, equ, J, params, initstate, conf)

    % Console output
    display('MechBox::mbEquilibirumSolver: ');
    display('    Calculating equilibrium solutions...');

    % Dimensions of the system
    Nx = numel(state);
    
    % Set the initial guess
    x_0 = initstate;

    % Configure the NR algorithm
    Nmaxiter = conf.MaxIterations;
    nr_errtol = conf.MaxError;

    % Initialize data
    fx = zeros(Nx,Nmaxiter);
    xcalc = zeros(Nx,Nmaxiter);
    xerr = zeros(Nmaxiter,1);
    xcalc(:,1) = x_0;

    % Run the Newton-Raphson algorithm
    for k = 1:Nmaxiter-1;

        fx(:,k) = equ(xcalc(:,k), params);
        Jx = J(xcalc(:,k), params);
        correction = -Jx\fx(:,k);
        xcalc(:,k+1) = xcalc(:,k) + correction;

        % Terminal condition
        xerr(k) = norm(correction,2);
        if xerr(k) <= nr_errtol
            break;
        end
    end

    % Save results
    Niter = k+1;
    err_eq = correction;
    equsol = xcalc(:,Niter);

    if Niter == Nmaxiter
        warning('mbEquilibirumSolver:: NR search for equillibirum states did not converge.');
    end

    display('    Done.');
    display('    NR number of iterations was: ');
    disp(  ['        : ' num2str(Niter)  ' [int]']);
    display('    NR final error was: ');
    disp(  ['        : [' num2str(err_eq.')  '] [units]']);
    display('    NR solutions is: ');
    disp(  ['        : [' num2str(equsol.')  '] [units]']);

    % Create plot of NR solution
    nrconvfig = figure('units','normalized','outerposition',[0 0 1 1]);
    nrconvfig.NumberTitle = 'off';
    nrconvfig.Name = 'MechBox: Newton-Raphson Convergence - Search for Equilibirum States';
    nrerraxes = axes(nrconvfig);
    nrconvaxes = axes(nrconvfig);

    subplot(1,2,1,nrerraxes);
    set(nrerraxes,'TickLabelInterpreter','latex','FontSize',12);
    %axis(nrconvaxes, 'equal'); 
    grid on;
    title('NR Correction/Error ','Interpreter','latex','FontSize',16)
    ylabel('$${\mid\mid \mathbf{x}_{eq} \mid\mid}_{2}$$','Interpreter','latex','FontSize',16);
    xlabel('Iterations [int]','Interpreter','latex','FontSize',16);
    nriter = 1:Niter;
    hold all;
    plot(nrerraxes,nriter,xerr(1:Niter));
    hold off;

    subplot(1,2,2,nrconvaxes);
    set(nrconvaxes,'TickLabelInterpreter','latex','FontSize',12);
    %axis(nrconvaxes, 'equal'); 
    grid on;
    title('Equilibirum State','Interpreter','latex','FontSize',16)
    ylabel('$$ \mathbf{x}_{eq} $$','Interpreter','latex','FontSize',16);
    xlabel('Iterations [int]','Interpreter','latex','FontSize',16);
    nriter = 1:Niter;
    hold all;
    for i = 1:Nx
        plot(nrconvaxes,nriter,xcalc(i,1:Niter));
    end
    hold off;

    for i = 1:Nx
        datanames{i} = mbSymbolic2Latex([],[],state(i),12,'latex');
    end
    nrconvlegend = legend(nrconvaxes,'show');
    nrconvlegend.Interpreter = 'latex';
    nrconvlegend.Box = 'on';
    nrconvlegend.String = datanames;
    nrconvlegend.FontSize = 14;

    % Reconstruct the original state (w/o fixed states)
    recond = isfield(conf, 'FixedStates') && ~isempty(conf.FixedStates)...
             && isfield(conf, 'FullState') && ~isempty(conf.FullState)...
             && isfield(conf, 'FullInitState') && ~isempty(conf.FullInitState);
    
    if recond
        
        Nq = numel(conf.FullState);
        alldofs = 1:Nq;
        [dummyvar, freedofs] = setdiff(conf.FullState, conf.FixedStates,'stable');
        fixeddofs = setdiff(alldofs, freedofs,'stable');
        Nfixed = numel(fixeddofs);
        
        equstate = zeros(Nq,1);

        for i = 1:Nfixed

            index = fixeddofs(i);

            if index ~=1 && index ~= Nq
                equstate(1:index-1,1) = equsol(1:index-1,1);
                equstate(index,1) = conf.FullInitState(index,1);
                equstate(index+1:Nq,1) = equsol(index:Nx,1);

            elseif index == 1
                equstate(1,1) = conf.FullInitState(1,1);
                equstate(2:Nq,1) = equsol;

            elseif index == Nq
                equstate(1:Nq-1,1) = equsol;
                equstate(Nq,1) = conf.FullInitState(Nq,1);

            end
        end 
        
    else
        equstate = equsol;
    end
    
    % Export image
    if isfield(conf, 'Print') && ~isempty(conf.Print)
       switch  conf.Print
           case 'pdf'
                set(nrconvfig,'Units','Inches');
                figdimensions = get(nrconvfig,'Position');
                set(nrconvfig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[figdimensions(3), figdimensions(4)]);
                filedatetime = datestr(now,'HH_MM_SS_mm_dd_yy');
                print(nrconvfig,filedatetime,'-dpdf','-r0');
           otherwise
               error('Error: mbSimulationVisualizer: Incorrect export type');
       end
        
    end
    
end
