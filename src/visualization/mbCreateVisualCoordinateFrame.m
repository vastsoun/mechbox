%
%   [csfarrows, csflabels] = mbCreateVisualCoordinateFrame(axes,objgroup,name,position,orientation,scale,fontsize)
%
%   Plots a 3D Cartesian coordinate frame with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [csfarrows, csflabels] = mbCreateVisualCoordinateFrame(axes,objgroup,name,position,orientation,scale,fontsize)

    % Process input arguments
    arrow_length = scale;
    csf_x = position(1); 
    csf_y = position(2);
    csf_z = position(3);
    csf_u = arrow_length*[0 1 0 0];
    csf_v = arrow_length*[0 0 1 0];
    csf_w = arrow_length*[0 0 0 1];
    
    % Create initial transfor group for pose initialization
    cstf = hgtransform('Parent',axes);
    
    % Add & initialize graphics objects to figure
    hold on;
    csfarrows(1) = quiver3('Parent',cstf,csf_x,csf_y,csf_z,csf_u(2),csf_v(2),csf_w(2),0);
    csfarrows(2) = quiver3('Parent',cstf,csf_x,csf_y,csf_z,csf_u(3),csf_v(3),csf_w(3),0);
    csfarrows(3) = quiver3('Parent',cstf,csf_x,csf_y,csf_z,csf_u(4),csf_v(4),csf_w(4),0);
    hold off;

    % Configure quiver frame format properties
    csfarrows(1).Color = 'red';
    csfarrows(2).Color = 'blue';
    csfarrows(3).Color = 'green';
    csfarrows(1).LineWidth = 1.5;
    csfarrows(2).LineWidth = 1.5;
    csfarrows(3).LineWidth = 1.5;
    csfarrows(1).MaxHeadSize = 0.2;
    csfarrows(2).MaxHeadSize = 0.2;
    csfarrows(3).MaxHeadSize = 0.2;

    % Render latex labels for csf arrows
    randmax = arrow_length/20;
    randoff = (randmax)*rand(4);
    csf_xlabel_pos = [csf_x;csf_x;csf_x;csf_x]+csf_u.'+randoff(1);
    csf_ylabel_pos = [csf_y;csf_y;csf_y;csf_y]+csf_v.'+randoff(2);
    csf_zlabel_pos = [csf_z;csf_z;csf_z;csf_z]+csf_w.'+randoff(3);
    
    % Create labels
    csf_olabel = strcat('$O_{',name,'}$');
    csf_xlabel = strcat('${\bf e}^{',name,'}_{x}$');
    csf_ylabel = strcat('${\bf e}^{',name,'}_{y}$');
    csf_zlabel = strcat('${\bf e}^{',name,'}_{z}$');
    csf_labels = {{csf_olabel},{csf_xlabel},{csf_ylabel},{csf_zlabel}};

    % Render the csf lebels
    axis_limits = axis(axes);
    if ~isempty(fontsize)
        csflabels = text(axes,csf_xlabel_pos,csf_ylabel_pos,csf_zlabel_pos,csf_labels,'Interpreter','latex','FontSize',fontsize);
    else    
        csflabels = text(axes,csf_xlabel_pos,csf_ylabel_pos,csf_zlabel_pos,csf_labels,'Interpreter','latex','FontSize',80*scale/(axis_limits(4)-axis_limits(3)));
    end
    
    % Initialzie to desired pose
    Txyz = makehgtform('translate', position);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(cstf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
        
    % Attach graphics objects to external transform parent object group
    csfarrows(1).Parent = objgroup;
    csfarrows(2).Parent = objgroup;
    csfarrows(3).Parent = objgroup;
    csflabels(1).Parent = objgroup;
    csflabels(2).Parent = objgroup;
    csflabels(3).Parent = objgroup;
    csflabels(4).Parent = objgroup;
    
end
