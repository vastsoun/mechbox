%
%   expr = mbRunMassFuncDS(extcallback,massfunc,t,states,auxstates,params)
%
%   ODE callback to compute the mass matrix of a system. The mass matrix
%   function must be provided by the user.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           1/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function expr = mbRunMassFuncDS(extcallback,massfunc,t,states,auxstates,params)

    % Determine if system is of type ODE or DAE
    if isempty(auxstates)
        % Execute the ODE dynamics implementation
        expr = massfunc(t,states,params);
    else
        % Execute the DAE dynamics implementation
        expr = massfunc(t,states,auxstates,params);
    end
    
    % If the callback handle is not empy, execute the callback operation.
    if ~isempty(extcallback)
        extcallback();
    end
    
end
