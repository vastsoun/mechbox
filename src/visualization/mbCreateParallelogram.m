%
%   obj = mbCreateParallelogram (axes,objgroup,dimensions,rgbcolor,transparency,position,orientation)
%
%   Generates and plots a 3D sphere with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreateParallelogram (axes,objgroup,dimensions,rgbcolor,transparency,position,orientation)

    % Create initial transfor group for pose initialization
    parallelogramtf = hgtransform('Parent',axes);

    % Define verticies for the parrallelogram sides
    x=([0 1 1 0 0 0;1 1 0 0 1 1;1 1 0 0 1 1;0 1 1 0 0 0]-0.5)*dimensions(1)+position(1)+dimensions(1)/2;
    y=([0 0 1 1 0 0;0 1 1 0 0 0;0 1 1 0 1 1;0 0 1 1 1 1]-0.5)*dimensions(2)+position(2)+dimensions(2)/2;
    z=([0 0 0 0 0 1;0 0 0 0 0 1;1 1 1 1 0 1;1 1 1 1 0 1]-0.5)*dimensions(3)+position(3)+dimensions(3)/2;
    
    % Create the sides
    for i = 1:6
        obj{i} = patch(axes,x(:,i),y(:,i),z(:,i),rgbcolor);
        set(obj{i},'edgecolor','k');
        obj{i}.Parent = parallelogramtf;
        obj{i}.FaceAlpha = transparency;
    end
    
    % Initialzie to desired pose
    Txyz = makehgtform('translate', position - [dimensions(1)/2 dimensions(2)/2 dimensions(3)/2].');
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(parallelogramtf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    for i = 1:6
        obj{i}.Parent = objgroup;
    end
    
end
