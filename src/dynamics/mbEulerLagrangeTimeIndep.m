%
%   function [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeIndep (tin, qin, Dqin, DDqin, L, Qin, Cin, conf)
%
%   Generates Equations of Motion with time-independent state vectors, i.e.
%   they are just symbolic variables
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           4/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [ EoM, q, Dq, DDq, Q, F, Lambda ] = mbEulerLagrangeTimeIndep(tin, qin, Dqin, DDqin, Lin, Qin, Cin, conf)

    %%%
    %
    %   Euler-Lagrange Equation (Type 1):
    %
    %   D/Dt(d/d(Dq)(L)) - d/d(q)(L) + J_c(q).'*Lambda = Qnc
    %
    %   Where:
    %           D/D(x)()    : Absolute derivative wrt to x
    %           d/d(x)()    : Partial derivative wrt to x
    %           J_c(q)      : Jacobian of constraints
    %           Lambda      : Vector of lagrangian multipliers
    %           Qnc         : Vector of non-conservative generalized forces
    %
    %%%
   
    % Extract the expression from the input - must onyl be a dependent
    % variable, i.e. not a function
    L = formula(Lin);
    
    % Calculate individual partial and absolute derivatives.
    dL_dq   = jacobian(L, qin).';
    dL_dDq  = jacobian(L, Dqin).';
    dL_dq = simplify(dL_dq);
    dL_dDq = simplify(dL_dDq);
    
    % Absolute derivative of the generalized momenta
    DdL_dDq = simplify(jacobian(dL_dDq, tin)) + simplify(jacobian(dL_dDq, qin))*Dqin + simplify(jacobian(dL_dDq, Dqin))*DDqin;
        
    % Formulate the Equations of Motion (EoM)
    if ~isempty(Qin)
        EoM = DdL_dDq - dL_dq - Qin;
    else
        EoM = DdL_dDq - dL_dq;
    end
    
    % Forumlate function return variables
    q = qin;
    Dq = Dqin;
    DDq = DDqin;
    Q = Qin;
    if ~isempty(Qin) && ~isfloat(Qin)
        F = argnames(Qin);
    else
        F = [];
    end
    Lambda = Cin;

end
