%
%   [data, time, perf] = mbSimulateDS(sysfunc, params, input, init, conf)
%
%   This function calculates the Equations of Motion (EoM) from a specified
%   Lagrangian function using the Euler-Lagrange equation.
%
%   Inputs:
%       sysfunc     Handle to function calculating and returning the
%                   differential terms of a dynamical system.
%       params      Values for the system parameters to be used in the
%                   simulation.
%       input       Input function which excites the system dynamics
%       init        Initial conditions for the system state
%       conf        Simulation configurations struct
%
%   Teturns:
%       data    
%       time
%       perf
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [data, time, perf] = mbSimulateDS(syseq, params, input, init, conf)

    % Tool console output
    display(' ');
    display('MechBox::DS Simulator: ');
    display('    Computing numerical solution...');

    %
    % Integrate fixed-variable step configuration
    %

    % Configure numerical solver;
    if isfield(conf, 'solverOptions')
        % Use user-specified solver options if defined
        solver_opts = conf.solverOptions;
    else
        % If user did not define, initialize an empty options array
        solver_opts = [];
    end
    
    % Augment the ODE function with a per-iteration callback handler
    if isfield(syseq,'M_func')
        massfunc = @(t,states) mbRunMassFuncDS([], syseq.M_func, t, states,[], params);
        solver_opts = odeset(solver_opts,'Mass',massfunc);
        sysfunc = syseq.F_func;
    else
        sysfunc = syseq; 
    end
    
    % Set time configurations for the simulation
    N = (conf.timeStop - conf.timeStart)/conf.timeStep;
    N = uint64(N);
    time = linspace(conf.timeStart, conf.timeStop, N);
    tspan = [conf.timeStart conf.timeStop];

    % Set the default ODE execution probe callback
    solver_opts = odeset(solver_opts,'OutputFcn',@mbProbeDS);
    
    % Set the solver type
    if isfield(conf, 'solverType')
    
        if strcmp(conf.solverType, 'ode45')
         
            % Augment the ODE function with a per-iteration callback handler
            if ~isempty(input)
                sysfunc = @(t,states) mbRunDS([], sysfunc, t, states,[], input(t), params);
            else
                sysfunc = @(t,states) mbRunFreeDS([], sysfunc, t, states,[], params);
            end
            
            % Set solver to standard ode45
            time_start = cputime;
            ode_sol = ode45(sysfunc, tspan, init.', solver_opts);
            
        elseif strcmp(conf.solverType, 'ode15s')
            
            % Augment the ODE function with a per-iteration callback handler
            if ~isempty(input)
                sysfunc = @(t,states) mbRunDS([], sysfunc, t, states,[], input(t), params);
            else
                sysfunc = @(t,states) mbRunFreeDS([], sysfunc, t, states,[], params);
            end
            
            % Use first-level stiff solver ode15s
            time_start = cputime;
            %solver_opts = odeset(solver_opts, 'RelTol', 1.0e-8, 'AbsTol' , 1.0e-8);
            ode_sol = ode15s(sysfunc, tspan, init.', solver_opts);
            
        elseif strcmp(conf.solverType, 'ode23s')
            
            % Augment the ODE function with a per-iteration callback handler
            if ~isempty(input)
                sysfunc = @(t,states) mbRunDS([], sysfunc, t, states,[], input(t), params);
            else
                sysfunc = @(t,states) mbRunFreeDS([], sysfunc, t, states,[], params);
            end
            
            % Use second level stiff solver ode23s
            time_start = cputime;
            %solver_opts = odeset(solver_opts, 'RelTol', 1.0e-8, 'AbsTol' , 1.0e-8);
            ode_sol = ode23s(sysfunc, tspan, init.', solver_opts);
            
        elseif strcmp(conf.solverType, 'ode15i')
            
            % Augment the DAE function with a per-iteration callback handler
            if ~isempty(input)
                sysfunc = @(t,states,auxstates) mbRunDS([], sysfunc, t, states, auxstates, input(t), params);
            else
                sysfunc = @(t,states,auxstates) mbRunFreeDS([], sysfunc, t, states, auxstates, params);
            end
                 
            %
            init_est = init;
            Dinit_est = zeros(numel(init),1);
            icopt = odeset('RelTol', 1.0e-9, 'AbsTol' , 1.0e-9);
            [sim_init, sim_Dinit] = decic(sysfunc, conf.timeStart, init_est, [], Dinit_est, [], icopt);
            
            time_start = cputime;
            ode_sol = ode15i(sysfunc, tspan, sim_init.', sim_Dinit.', solver_opts);
            
        else
            % Signal for error due to incorrect solver spec
            error('error: mbSimulateODE: Incorrect or no solver has not been specified, please define the .solverType field correctly');
            
        end
    
    else
        
        % Default solver is ode45
        time_start = cputime;
        ode_sol = ode45(sysfunc, tspan, init.', solver_opts);
        
    end
    
    % Check if solver produced a different tha time range
    Nsol = size(ode_sol.x,2);
    if ode_sol.x(Nsol) < time(N)
        time = linspace(ode_sol.x(1), ode_sol.x(Nsol), N);
    end
    
    % Evaluate the solution in the desired time-range
    data = deval(ode_sol, time);
    time_end = cputime - time_start;

    % Collect performance timer data
    perf = time_end;

    % Notify when simulation has completed;
    if conf.notification == true
        load gong
        sound(y,Fs)
    end

    % Tool console output
    disp(  ['    Using solver:                 ' num2str(conf.solverType)]);
    disp(  ['    Solver run-time duration is:  ' num2str(perf)  ' sec']);
    
end

% function S = jpattern(N)
%     % Jacobian sparsity pattern
%     B = ones(2*N,5);
%     B(2:2:2*N,2) = zeros(N,1);
%     B(1:2:2*N-1,4) = zeros(N,1);
%     S = spdiags(B,-2:2,2*N,2*N);
% end
