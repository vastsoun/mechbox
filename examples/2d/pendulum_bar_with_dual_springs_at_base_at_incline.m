%%
%   File:           pendulum_bar_with_dual_springs_at_base_at_incline.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Desciption:     Analysis of a system with a single shaft/bar as a
%                   pendulum and which is suspended from a horizontally
%                   mving base which slides along a linear guide, and is
%                   supported by a spring on either side.
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [-g 0 0].';

%% Define Bodies

% Define massess
syms m_1 m_2 m_3 real;

% Model parameters
syms k L mu alpha real;

% Moment of inertia wrt to CoM
I_1 = [0 0 0; 0 0 0; 0 0 0].';
I_2 = [0 0 0; 0 0 0; 0 0 (1/12)*m_2*L^2].';
I_3 = [0 0 0; 0 0 0; 0 0 m_3*L^2].';

%% Define Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame.
%   {P_n,i_n,j_n,k_n} - Local frame at point P_n. 
%%%

% Define absolute coodrinates
syms x_1 y_1 theta_1 real;      % CoM of slider m_1
syms x_2 y_2 theta_2 real;      % CoM of pendulum shaft m_2
syms x_3 y_3 theta_3 real;      % CoM of pendulum mass m_3

% Define absolute velocities
syms Dx_1 Dy_1 Dtheta_1 real; 
syms Dx_2 Dy_2 Dtheta_2 real; 
syms Dx_3 Dy_3 Dtheta_3 real; 

% Define absolute accelerations
syms DDx_1 DDy_1 DDtheta_1 real; 
syms DDx_2 DDy_2 DDtheta_2 real; 
syms DDx_3 DDy_3 DDtheta_3 real; 

%% Absolute Coordinates

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_1 y_1 theta_1 x_2 y_2 theta_2 x_3 y_3 theta_3].';
Du = [Dx_1 Dy_1 Dtheta_1 Dx_2 Dy_2 Dtheta_2 Dx_3 Dy_3 Dtheta_3].';
DDu = [DDx_1 DDy_1 DDtheta_1 DDx_2 DDy_2 DDtheta_2 DDx_3 DDy_3 DDtheta_3].';

%% System Constraints

% Holonomic Constraints
f_1 = x_1;
f_2 = theta_1;
f_3 = x_3*tan(theta_3) - y_3 + y_1;
f_4 = x_3^2 + (y_3-y_1)^2 - (L)^2;

% Velocity form of holonomic-constraints in absolute coordinates
Df_1 = Dy_1;
Df_2 = Dtheta_1;
Df_3 = Dx_3*tan(theta_3) + x_2*Dtheta_3*((cos(theta_3))^(-2)) - Dy_3;
Df_4 = 2*x_3*Dx_3 + 2*(y_3 - y_1)*(Dy_3 - Dy_1);

% Non-holonomic Constraints
% NONE

%% Generalized Coordinates

% Generazied quantities
syms q Dq DDq Q real;

% Generalized displacements
syms theta y real;
syms Dtheta Dy real;
syms DDtheta DDy real;

% Define the generalized quantities
q = [y theta].';
Dq = [Dy Dtheta].';
DDq = [DDy DDtheta].';

%% Forward Kinematics 

%
theta_1 = 0;
theta_2 = theta;
theta_3 = theta;

%
x_1 = 0;
y_1 = y;
x_2 = (L/2)*cos(theta_2);
y_2 = (L/2)*sin(theta_2) + y;
x_3 = (L)*cos(theta_3);
y_3 = (L)*sin(theta_3) + y;

%
Dtheta_1 = 0;
Dtheta_2 = Dtheta;
Dtheta_3 = Dtheta;

%
Dx_1 = 0;
Dy_1 = Dy;
Dx_2 = - (L/2)*Dtheta_2*sin(theta_2);
Dy_2 = (L/2)*Dtheta_2*cos(theta_2) + Dy;
Dx_3 = - (L)*Dtheta_3*sin(theta_3);
Dy_3 = (L)*Dtheta_3*cos(theta_3) + Dy;

% Absolute position vectors
r_1 = [x_1 y_1 0].';
r_2 = [x_2 y_2 0].';
r_3 = [x_3 y_3 0].';

% Absolute velocity vectors
v_1 = [Dx_1 Dy_1 0].';
v_2 = [Dx_2 Dy_2 0].';
v_3 = [Dx_3 Dy_3 0].';

% Angular velocities of CoM 
omega_1 = [0 0 Dtheta_1].';
omega_2 = [0 0 Dtheta_3].';
omega_3 = [0 0 Dtheta_3].';

%% External Forces/Torques

% This system has a single external torque only
syms F_y_ext T_theta_ext real;

%
syms T F real;

% Define the exernal force in the dimensions of the absolute linear
% displacements
Targs = T_theta_ext;

% DDefine the exernal force in the dimensions of the absolute angular
% displacements
Fargs = [];

%% Generalized Forces

% Define the generalized forces from absolute forces acting at point D
syms Q_y Q_theta real;

% Define external forces in the dimension of each generalized coordinate
Q_y = -mu*m_1*g*sign(Dy_1); % Coulomb kinetic friction
Q_theta = T_theta_ext;      % Externally applied torque

% Generalized external forces as a function of the absolute external forces
Q_sys([Fargs Targs].') = [Q_y Q_theta].';

%% Kinetics

% Kinetic energy terms
T_1 = (1/2)*m_1*(v_1.'*v_1) + (1/2)*omega_1.'*I_1*omega_1;
T_2 = (1/2)*m_2*(v_2.'*v_2) + (1/2)*omega_2.'*I_2*omega_2;
T_3 = (1/2)*m_3*(v_3.'*v_3) + (1/2)*omega_3.'*I_3*omega_3;

% Total kinetic energy 
T_sys = T_1 + T_2 + T_3;
T_sys = simplify(T_sys, 'Steps', 100);

% Elastic potential energy terms
V_k1 = (1/2)*k*(y_1)^2;
V_k2 = (1/2)*k*(y_1)^2;

% Gravitational potential energy terms
V_g1 = m_1*r_1.'*mbRotationMatrix('z', alpha) * e_g;
V_g2 = m_2*r_2.'*mbRotationMatrix('z', alpha) * e_g;
V_g3 = m_3*r_3.'*mbRotationMatrix('z', alpha) * e_g;

% Total potential energy 
V_sys = V_k1 + V_k2 + V_g1 + V_g2 + V_g3;
V_sys = simplify(V_sys, 'Steps', 100);

%% System Lagrangian

% System Lagrangian[q, Dq, DDq]
L_sys(t, q.', Dq.') = T_sys - V_sys;
L_sys = simplify(L_sys, 'Steps', 100);

% Display resulting Lagrangian function
display('System Lagrangian: ');
pretty(L_sys == 0);

%% Generate EoM

% Constraints
C_sys = [];

% Generator configurations
eulagconf.Type = 'TimeDependent';

% Compute EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

% Display resulting EoM
display('Equations of Motion: ');
pretty(EoM);

%% Generate System ODE

% Set which variables represent system parametes
sysparams = [g m_1 m_2 m_3 L mu k alpha].';

% Generator configurations
dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, Dsysvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Numerical Solutions

% Set the simulation parameters for the system
%siminput = @(t) sin(t);
siminput = @(t) 0.0;
simparams = [9.81 1.0 0.01 1.0 0.5 0.1 100.0 -pi/8].';
siminit = [0.0 pi/8 0.0 0.0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 5.0;
simconf.timeStep = 1e-4;
simconf.solverType = 'ode45';
simconf.notification = true;

% Run the Simulation
[simdata, simtime, solverTime] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Define the Simulation Environment

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_1 = @(input,params) [0; input(1); 0.0; 0.0; 0.0; 0.0];
tf_2 = @(input,params) [(params(5)/2)*cos(input(2)); input(1) + (params(5)/2)*sin(input(2)); 0.0; 0.0; 0.0; input(2)];
tf_3 = @(input,params) [params(5)*cos(input(2)); input(1) + params(5)*sin(input(2)); 0.0; 0.0; 0.0; input(2)];

bodies(1).tfs = tf_1;
bodies(1).shapePrimitiveType = 'parallelogram';
bodies(1).shapePrimitiveParams = [0.06 0.06 0.06];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).name = '1';
bodies(1).showCS = true;
bodies(1).showTrail = true;

bodies(2).tfs = tf_2;
bodies(2).shapePrimitiveType = 'line';
bodies(2).shapePrimitiveParams = [simparams(5) 2.0];
bodies(2).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(2).name = '2';
bodies(2).showCS = true;
bodies(2).showTrail = true;

bodies(3).tfs = tf_3;
bodies(3).shapePrimitiveType = 'sphere';
bodies(3).shapePrimitiveParams = 0.02;
bodies(3).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(3).name = '3';
bodies(3).showCS = true;
bodies(3).showTrail = true;

%% Run the Simulation Visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = 0.5 * [-0.2 1.8; -1 1; -1 1];
simvizconf.fps = 30.0;  
simvizconf.camlight = true;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.15;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 20;

% Run the visualization
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% SimulationOverview

simovconf.FontSize = 22;
simovconf.DataSet = [];
simovconf.Print = [];

mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
