%%
%   File:           3dof_articulated_leg.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           8/12/2016
%
%   Desciption:     Analytical derivation of the kinematic model of a 
%                   single-leg of a quadruped robot.
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [0 0 g].';

%% Define Bodies

% Define massess
syms m_B m_H m_T m_S real;
assume(m_B>0 & m_H>0 & m_T>0 & m_S>0);

% Geometric parameters
syms d_Hx d_Hy d_Hz real;
syms d_Tx d_Ty d_Tz real;
syms d_Sx d_Sy d_Sz real;
syms d_Fx d_Fy d_Fz real;

% Moment of inertia w.r.t. to CoM
Theta_B =  [0 0 0; 
            0 0 0; 
            0 0 0];
Theta_H =  [0 0 0; 
            0 0 0; 
            0 0 0];
Theta_T =  [0 0 0; 
            0 0 0; 
            0 0 0];
Theta_S =  [0 0 0; 
            0 0 0; 
            0 0 0];
Theta_F =  [0 0 0; 
            0 0 0; 
            0 0 0];

%% Define Coordinates & Coordinate Frames

%%%
%   We define the following reference frames:
%
%   {O_I,e_I_x,e_I_y,e_I_z} - Global inertial frame.
%   {O_n,e_n_x,e_n_y,e_n_z} - Local frame at origin point O_n. 
%%%

% Define absolute positions
syms x_B y_B z_B real;    % CoM of base
syms x_H y_H z_H real;    % CoM of hip
syms x_T y_T z_T real;    % CoM of thigh
syms x_S y_S z_S real;    % CoM of shank
syms x_F y_F z_F real;    % CoM of foot

%% Joint DoFs

% Joint angles for each actuator
syms phi_HAA phi_HFE phi_KFE real;

% Joint anglular velocities for each actuator
syms Dphi_HAA Dphi_HFE Dphi_KFE real;

% Joint anglular accelerations for each actuator
syms DDphi_HAA DDphi_HFE DDphi_KFE real;

%% System parameters

% Acceleration of gravity in m/s^2
pGlobal         = [ g ].';
pMasses         = [ m_B m_H m_T m_S ].';
pFrameOffsets   = [ d_Hx d_Hy d_Hz ...
                    d_Tx d_Ty d_Tz ...
                    d_Sx d_Sy d_Sz ...
                    d_Fx d_Fy d_Fz].';
                
% Array of parametes for the total system
sysparams   = [ pGlobal;
                pMasses;
                pFrameOffsets];

% Set parameter values
valGlobal       = [ 9.81 ].';
valMasses       = [ 1 1 1 1 ].';
valFrameOffsets = [ 0 0.05 0 ...
                    0 0 -0.1 ...
                    0 0 -0.2 ...
                    0 0 -0.2].';
                
% Array of parametes values
legparams    = [ valGlobal;
                valMasses;
                valFrameOffsets];

%% Generalized Coordinates

% Define the generalized motion vectors
q = [phi_HAA phi_HFE phi_KFE].';
Dq = [Dphi_HAA Dphi_HFE Dphi_KFE].';
DDq = [DDphi_HAA DDphi_HFE DDphi_KFE].';

% Plot the generalized coordinate vector
%mbSymbolic2Latex([],'\mathbf{q} := ',q,18,'plot');

%% Forward Kinematics 

%%%
%   TODO:
%   
%   1. Incorporate the axes misalignment such that the C_XY between frames
%   also takes into account static differences in relative orientation
%   between body frames.
%
%   2. Consider also the motion of the base.
%
%   3. Orientation is parametrized using the joint angles.
%
%%%

% Rotation matrices between CS frames of leg
C_BH = [1 0 0; 
        0 cos(phi_HAA) -sin(phi_HAA); 
        0 sin(phi_HAA) cos(phi_HAA)];
C_HT = [cos(phi_HFE) 0 sin(phi_HFE);
        0 1 0;
        -sin(phi_HFE) 0 cos(phi_HFE)];
C_TS = [cos(phi_KFE) 0 sin(phi_KFE);
        0 1 0;
        -sin(phi_KFE) 0 cos(phi_KFE)];
C_SF = [1 0 0; 
        0 1 0;
        0 0 1];
    
% Plot the rotation matrices
%mbSymbolic2Latex([],'C_{BH} := ',C_BH,22,'plot');

% Position of succesive joint CSs in local frames
r_BH_inB = [d_Hx d_Hy d_Hz].';
r_HT_inH = [d_Tx d_Ty d_Tz].';
r_TS_inT = [d_Sx d_Sy d_Sz].';
r_SF_inS = [d_Fx d_Fy d_Fz].';

% Respective homogeneous transformations
H_BH = [C_BH r_BH_inB;
        zeros(1,3) 1];
    
H_HT = [C_HT r_HT_inH;
        zeros(1,3) 1];
    
H_TS = [C_TS r_TS_inT;
        zeros(1,3) 1];
    
H_SF = [C_SF r_SF_inS;
        zeros(1,3) 1];
    
% Base-to-foot homogeneous transformation
H_BS = H_BH * H_HT * H_TS;

% Base-to-foot relative position
hom_r_BF_inB = H_BS * [r_SF_inS.' 1].';
r_BF_inB = hom_r_BF_inB(1:3,1);

%% Forward Differntial Kinematics 

%%%
%   NOTES:
%
%   1. Since J_BF_inB is in R^{3x3}, then it is invertible while the
%   configuration does not coincide with any of the kinematic
%   singularities.
%
%%%

% Compute the jacobian of the Base-to-foot relative position w.r.t the
% generalized coordinates - i.e. the configuration Jacobian
J_BF_inB = jacobian(r_BF_inB, q);

% Compute the relative velocity of the foot w.r.t the B-frame
v_BF_inB = J_BF_inB*Dq;

%% Numerical Representations for Forward Kinematics

% Create numerical solver for the forward kinematics
func_r_BF_inB = matlabFunction(r_BF_inB, 'Vars', {q, sysparams});
func_J_BF_inB = matlabFunction(J_BF_inB, 'Vars', {q, sysparams});
func_v_BF_inB = matlabFunction(v_BF_inB, 'Vars', {q, Dq, sysparams});

%% Inverse Kinematics 

%%%
%   NOTE:
%
%   1.  This algorithm requries the following arguments:
%
%       i.   func_r_BF_inB() function handle.
%       ii.  func_J_BF_inB() function handle.
%       iii. q_current
%       iv.  r_goal_inB
%       v.   r_error_max (optional).
%
%   2.  The termination condition for Newton-Raphson gradient descent is
%       just the Euclidean norm of the error in position.
%
%%%

% Set the current known configuration
q_current = [0 pi/10 -pi/10].';

% Specify goal/target position
r_goal_inB = [0 0.05 -0.3].';

% Compute the current position and current error from goal
r_current_inB = func_r_BF_inB(q_current, legparams);
r_error = r_goal_inB - r_current_inB;

% Set the max error tolerance
r_error_max = 1e-9;

% Initial guess is the current configuration
q_iter(:,1) = q_current;
k = 1;

% Run an implementation of Newton-Raphson Gradient Descent
while (norm(r_error,2) > r_error_max)

    % Compute position and jacobian in current iteration
    r_iter_inB = func_r_BF_inB(q_iter(:,k), legparams);
    J_iter = func_J_BF_inB(q_iter(:,k), legparams);
    
    % Compute the new configuration estimate
    q_iter(:,k+1) = q_iter(:,k) + J_iter\(r_goal_inB - r_iter_inB);
    r_error = r_goal_inB - func_r_BF_inB(q_iter(:,k+1), legparams);
    k = k + 1;
end

% Store solution
Niter = k;
r_solution_inB = r_iter_inB;
q_solution = q_iter(:,k);

% Clear iteration variables
clear q_iter J_iter r_iter_inB;

%% Trajectory Following using Inverse Differential Kinematics

% Initial state of the robot
q_0 = [pi/10 pi/10 -pi/10].';
Dq_0 = zeros(3,1);

% Configure the controller parmeters
K_p = 1;

% Desired trajectory parameters
traj_circle_center = r_goal_inB;
traj_circle_radius = 0.1;
traj_circle_freq = 0.25;

% Create trajectory generator functions
r_goal = @(t) traj_circle_center + traj_circle_radius*[sin(2*pi*traj_circle_freq*t), cos(2*pi*traj_circle_freq*t), -0.3]';
v_goal = @(t) (2*pi) * (traj_circle_freq * traj_circle_radius) * [cos(2*pi*traj_circle_freq*t),0,-sin(2*pi*traj_circle_freq*t)]';

% Define here the time resolution
deltaT = 0.01;
time_data = 0:deltaT:(2/traj_circle_freq);

% q, r, and r_goal are stored for every point in time in the following arrays
q_data = zeros(3,length(time_data));
Dq_data = zeros(3,length(time_data));
r_data = zeros(3,length(time_data));
r_goal_data = zeros(3,length(time_data));

% Run the numerical simulation
q_iter = q_0;
Dq_iter = Dq_0;
for i = 1:length(time_data)
    
    % Update step
    t = time_data(i);
    q_iter = q_iter + Dq_iter*deltaT;
    r_iter = func_r_BF_inB(q_iter, legparams);
    r_goal_iter = r_goal(t);
    
    % Run the controller     
    J_iter = func_J_BF_inB(q_iter, legparams);
    v_iter = K_p*(r_goal_iter - r_iter);
    Dq_iter = J_iter\v_iter;
    
    % Logging
    q_data(:,i) = q_iter;
    Dq_data(:,i) = Dq_iter;
    r_data(:,i) = r_iter;
    r_goal_data(:,i) = r_goal_iter;
end

%% Kinematics for the Visualization

% Compute states of the rest of the bodies
H_BT = H_BH * H_HT;
hom_r_BS_inB = H_BT * [r_TS_inT.' 1].';
hom_r_BT_inB = H_BH * [r_HT_inH.' 1].';
r_BT_inB = hom_r_BT_inB(1:3,1);
r_BS_inB = hom_r_BS_inB(1:3,1);

r_HT_link_inB = r_BH_inB + (r_BT_inB - r_BH_inB)/2;
r_TS_link_inB = r_BT_inB + (r_BS_inB - r_BT_inB)/2;
r_SF_link_inB = r_BS_inB + (r_BF_inB - r_BS_inB)/2;

C_BH = [1 0 0; 
        0 cos(phi_HAA) -sin(phi_HAA); 
        0 sin(phi_HAA) cos(phi_HAA)];
C_HT = [cos(phi_HFE) 0 sin(phi_HFE);
        0 1 0;
        -sin(phi_HFE) 0 cos(phi_HFE)];
C_TS = [cos(phi_KFE) 0 sin(phi_KFE);
        0 1 0;
        -sin(phi_KFE) 0 cos(phi_KFE)];
C_SF = [1 0 0; 
        0 1 0;
        0 0 1];
    
C_BT = C_BH*C_HT;
C_BS = C_BH*C_HT*C_TS;
C_BF = C_BH*C_HT*C_TS*C_SF;

% Compute angle-axis
[u_BH, theta_BH] = mbRotationMatrixToAxisAngle(C_BH);
[u_BT, theta_BT] = mbRotationMatrixToAxisAngle(C_BT);
[u_BS, theta_BS] = mbRotationMatrixToAxisAngle(C_BS);
[u_BF, theta_BF] = mbRotationMatrixToAxisAngle(C_BF);

% Compute the poses of each joint CS-frame
pose_IB_inB = [[0 0 0] [0 0 1 0]].';
pose_BH_inB = [r_BH_inB.' [0 0 1 0]].';
pose_BT_inB = [r_BT_inB.' u_BT.' theta_BT].';
pose_BS_inB = [r_BS_inB.' u_BS.' theta_BS].';   
pose_BF_inB = [r_BF_inB.' u_BF.' theta_BF].';

% Compute the poses of each link
pose_HT_link_inB = [r_HT_link_inB.' u_BH.' theta_BH].';
pose_TS_link_inB = [r_TS_link_inB.' u_BT.' theta_BT].';
pose_SF_link_inB = [r_SF_link_inB.' u_BS.' theta_BS].';

% pose_BH_inB = [r_BH_inB.' [0 0 0]].';
% pose_BT_inB = [r_BT_inB.' [phi_HAA 0 0]].';
% pose_BS_inB = [r_BS_inB.' [phi_HAA phi_HFE 0]].';   
% pose_BF_inB = [r_BF_inB.' [phi_HAA (phi_HFE + phi_KFE) 0]].';

% pose_HT_link_inB = [r_HT_link_inB.' [phi_HAA 0 0]].';
% pose_TS_link_inB = [r_TS_link_inB.' [phi_HAA phi_HFE 0]].';
% pose_SF_link_inB = [r_SF_link_inB.' [phi_HAA (phi_HFE + phi_KFE) 0]].';

%% Multi-Rigid-Body Kinematics Generator Functions

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_B = @(state,params) [[0 0 0] [0 0 1 0]].';
tf_H = matlabFunction(pose_BH_inB, 'Vars', {[q; Dq].', sysparams});
tf_T = matlabFunction(pose_BT_inB, 'Vars', {[q; Dq].', sysparams});
tf_S = matlabFunction(pose_BS_inB, 'Vars', {[q; Dq].', sysparams});
tf_F = matlabFunction(pose_BF_inB, 'Vars', {[q; Dq].', sysparams});
tf_HT_link = matlabFunction(pose_HT_link_inB, 'Vars', {[q; Dq].', sysparams});
tf_TS_link = matlabFunction(pose_TS_link_inB, 'Vars', {[q; Dq].', sysparams});
tf_SF_link = matlabFunction(pose_SF_link_inB, 'Vars', {[q; Dq].', sysparams});

%% Define Visualization Environment

% Bodies interacting dynamically

% Base
bodies(1).tfs = tf_B;
bodies(1).shapePrimitiveType = 'parallelogram';
bodies(1).shapePrimitiveParams = [0.05 0.05 0.05];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).showCS = true;
bodies(1).showTrail = false;
bodies(1).name = 'B';

% Joint Frames

bodies(2).tfs = tf_H;
bodies(2).shapePrimitiveType = 'cylinder';
bodies(2).shapePrimitiveParams = [0.015 0.05];
bodies(2).shapeColor = [0.8 0.3 0.3 0.3];
bodies(2).shapeOffsets = [0 0 0 0 pi/2 0].';
bodies(2).showCS = true;
bodies(2).showTrail = false;
bodies(2).name = 'HHA';

bodies(3).tfs = tf_T;
bodies(3).shapePrimitiveType = 'cylinder';
bodies(3).shapePrimitiveParams = [0.015 0.05];
bodies(3).shapeColor = [0.8 0.3 0.3 0.3];
bodies(3).shapeOffsets = [0 0 0 pi/2 0 0].';
bodies(3).showCS = true;
bodies(3).showTrail = false;
bodies(3).name = 'HFE';

bodies(4).tfs = tf_S;
bodies(4).shapePrimitiveType = 'cylinder';
bodies(4).shapePrimitiveParams = [0.015 0.05];
bodies(4).shapeColor = [0.8 0.3 0.3 0.3];
bodies(4).shapeOffsets = [0 0 0 pi/2 0 0].';
bodies(4).showCS = true;
bodies(4).showTrail = false;
bodies(4).name = 'KFE';

bodies(5).tfs = tf_F;
bodies(5).shapePrimitiveType = 'sphere';
bodies(5).shapePrimitiveParams = 0.02;
bodies(5).shapeColor = [0.8 0.3 0.3 0.3];
bodies(5).showCS = true;
bodies(5).showTrail = true;
bodies(5).name = 'F';

% Link Bodies

bodies(6).tfs = tf_HT_link;
bodies(6).shapePrimitiveType = 'cylinder';
bodies(6).shapePrimitiveParams = [0.01 0.1];
bodies(6).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(6).showCS = false;
bodies(6).showTrail = false;
bodies(6).name = 'H';

bodies(7).tfs = tf_TS_link;
bodies(7).shapePrimitiveType = 'cylinder';
bodies(7).shapePrimitiveParams = [0.01 0.2];
bodies(7).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(7).showCS = false;
bodies(7).showTrail = false;
bodies(7).name = 'T';

bodies(8).tfs = tf_SF_link;
bodies(8).shapePrimitiveType = 'cylinder';
bodies(8).shapePrimitiveParams = [0.01 0.2];
bodies(8).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(8).showCS = false;
bodies(8).showTrail = false;
bodies(8).name = 'S';

%% Run the Simulation Visualization

% Configure visualization data
simparams = legparams;
simdata = [q_data.' Dq_data.'].';
simtime = time_data;

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = 0.4*[-1 1; -1 1; -1.5 0.5];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.1;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 18;

% Run the visualizers
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% Simulation Overivew

simovconf.FontSize = 18;
simovconf.DataSet = [1 2 3; 4 5 6];
mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
