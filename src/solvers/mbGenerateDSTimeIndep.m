%
%   function [sysf, vars, Dvars] = mbGenerateDSTimeDep(equ, params, t, q, Dq, DDq, F, Lambda, conf)
%
%   This function generates a dynamical system representation from a set of
%   EoM, which can then be solved using numerical solvers.
%
%   inputs:
%       equ     Symbolic expression of the system equations
%       params  Vector of symbolic variables representing system parameters
%       t       Symbolic variable for time
%       q       Symbolic vector of generalized coordinates
%       F       Vector of parametrized external forces
%       Lambda  Vector of Lagrangian multipliers for constrained systems
%
%   returns:
%       sysf    Function handle to the system dynamics generator
%       vars    Vector of symbolic state-variables for the generated system
%       Dvars   Vector of state variables expressed as functions of 
%               derivatives of other state variables.
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           1/7/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [sysf, vars, Dvars] = mbGenerateDSTimeIndep(equ, params, t, q, Dq, DDq, F, Lambda, conf)

    %%%
    %   State-Space Representation of the Dynamical System:
    %
    %  [ I   0    ] [ Dq  ]   [  Dq   ]
    %  [ 0   M(q) ] [ DDq ] = [ F_sys ]
    %
    %%%

    % Check if we are solving constrained EoM
    if isempty(Lambda)
        
        % Simplify expressions
        %equations = simplify(equ, 'Steps', 100);
        equations = equ;
        
        % System Mass Matrix
        M_sys = formula(jacobian(equations, DDq));
        
        % System Forces/Dynamics
        F_sys = formula(M_sys*DDq - equations);
        
        % Formulate DS-ODE augmented mass matrix
        M_ds = [eye(size(M_sys)) zeros(size(M_sys)); zeros(size(M_sys)) M_sys];
        
        % Formulate DS-ODE augmented system dynamics
        F_ds = [Dq; F_sys];
        
        % Generate MATLAB functions which implement the system dynamics
        if ~isempty(conf) && isfield(conf, 'createFile')
            if conf.createFile == true
                if ~isempty(F)
                    sysf.M_func = matlabFunction(M_ds, 'file', 'dsMassMatrixFunc',  'vars', {t, [q; Dq], params});
                    sysf.F_func = matlabFunction(F_ds, 'file', 'dsDynamicsFunc',    'vars', {t, [q; Dq], F, params});
                else
                    sysf.M_func = matlabFunction(M_ds, 'file', 'dsMassMatrixFunc',  'vars', {t, [q; Dq], params});
                    sysf.F_func = matlabFunction(F_ds, 'file', 'dsDynamicsFunc',    'vars', {t, [q; Dq], params});
                end
            end
        else
             if ~isempty(F)
                sysf.M_func = matlabFunction(M_ds, 'vars', {t, [q; Dq], params});
                sysf.F_func = matlabFunction(F_ds, 'vars', {t, [q; Dq], F, params});
            else
                sysf.M_func = matlabFunction(M_ds, 'vars', {t, [q; Dq], params});
                sysf.F_func = matlabFunction(F_ds, 'vars', {t, [q; Dq], params});
            end
        end
        
        % Define the system variables
        vars = [q; Dq];
        Dvars = [Dq; DDq];
        
    % If lambdas have been specified, then the EoM are uncconstrained
    else
        
        %
        % TODO
        %
        
    end

end
