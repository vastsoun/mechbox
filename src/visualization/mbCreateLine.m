%
%   obj = mbCreateLine (axes,objgroup,length,thickness,rgbcolor,position,orientation)
%
%   Generates and plots a 3D line with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreateLine (axes,objgroup,length,thickness,rgbcolor,position,orientation)

    % Create initial transfor group for pose initialization
    linetf = hgtransform('Parent',axes);

    % Initialize point trails
    X = linspace(-length/2,length/2);
    Y = zeros(size(X,1),size(X,2));
    Z = zeros(size(X,1),size(X,2));
    
    % Create graphics object
    obj = animatedline(axes,X,Y,Z);
    obj.Parent = linetf;
    obj.Color = rgbcolor;
    obj.LineWidth = thickness;
    
    % Initialzie to desired pose
    Txyz = makehgtform('translate', position);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(linetf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    obj.Parent = objgroup;

end
