# Mechanics Toolbox

## Installation

Just add to MATALB's path.

## Examples

The `./examples/` directory contains a series of simple mechanical systems to get started.

----
Copyright (C) 2016, Vassilios Tsounis. All rights reserved.
