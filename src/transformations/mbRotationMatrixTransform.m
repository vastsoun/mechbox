%
%   function [ output_vector ] = mbRotationMatrix( axis, angle, input_vector )
%
%   This function performs a 1D rotation of a input vector about one of the
%   XYZ axes using a 3x3 rotation matrix. The function supports both 
%   symbolic and numerical operations.
%
%   inputs:
%       axis            Relative axis about which to rotate the input vector.dd This can be 'x', 'y or 'z'.
%       angle           Symbolic or numeric axis variable by which to rotate the input vector
%       input_vector    Symbolic or numeric 3D input vector
%
%   returns:
%       output_vector   Rotated symbolic or numeric 3D vector.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [ output_vector ] = mbRotationMatrixTransform( axis, angle, input_vector )

    % Check type validity of input argument
    assert(isa(axis,'char'),'Error: "axis" argument is not a "char"');

    % Check dimension of symbolic input arguments
    assert(size(angle, 1) == 1,'Error: "angle" argument is not scalar');
    assert(size(input_vector, 1) == 3,'Error: "input_vector" argument is not 3x1 array');

    % Determine the type of rotation to apply
    if      axis == 'x'
        output_vector = RotX(angle,input_vector);

    elseif  axis == 'y'
        output_vector = RotY(angle,input_vector);

    elseif  axis == 'z'
        output_vector = RotZ(angle,input_vector);

    else
        error('Incorrect argument value for `axis` input.')
    end

end

%% Support Functions

function [ output_vector ] = RotX( angle, input_vector )

    R = [1 0 0;
         0 cos(angle) -sin(angle);
         0 sin(angle) cos(angle)];

    output_vector = R * input_vector;

end

function [ output_vector ] = RotY( angle, input_vector )

    R = [cos(angle) 0 sin(angle);
         0 1 0;
         -sin(angle) 0 cos(angle)];

    output_vector = R * input_vector;

end

function [ output_vector ] = RotZ( angle, input_vector )

    R = [cos(angle) -sin(angle) 0;
         sin(angle) cos(angle)  0;
         0 0 1];

    output_vector = R * input_vector;

end

%% EOF
