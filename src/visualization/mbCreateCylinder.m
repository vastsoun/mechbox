%
%   obj = mbCreateCylinder(axes,objgroup,radius,height,rgbcolor,transparency,position,orientation)
%
%   Generates and plots a 3D cylinder with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreateCylinder(axes,objgroup,radius,height,rgbcolor,transparency,position,orientation)

    % Create initial transfor group for pose initialization
    cylindertf = hgtransform('Parent',axes);

    % Create the cylindrical shaft
    tcyl = zeros(10,1) + radius;
    [X,Y,Z] = cylinder(tcyl,50);
    C = zeros(size(X,1),size(X,2));
    obj(1) =  mbCreateMesh(axes,X,Y,height*Z,C);
    obj(1).Parent = cylindertf;
    obj(1).EdgeColor = 'none';
    obj(1).FaceColor = rgbcolor;
    obj(1).FaceAlpha = transparency;
    obj(1).FaceLighting = 'gouraud';

    % Define data ranges
    azimuth = linspace(0, 2*pi);
    zl = zeros(1,size(azimuth,2));
    zh = ones(1,size(azimuth,2))*height;
    cylx = radius*cos(azimuth);
    cyly = radius*sin(azimuth);

    % Create bottom surface
    obj(2) = patch(axes, cylx, cyly, zl,rgbcolor);
    obj(2).Parent = cylindertf;
    obj(2).FaceAlpha = transparency;
    obj(2).FaceLighting = 'gouraud';

    % Create top surface
    obj(3) = patch(axes, cylx, cyly, zh,rgbcolor);
    obj(3).Parent = cylindertf;
    obj(3).FaceAlpha = transparency;
    obj(3).FaceLighting = 'gouraud';

    % Initialzie to desired pose
    Txyz = makehgtform('translate', position.' - [0 0 height/2]);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(cylindertf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    obj(1).Parent = objgroup;
    obj(2).Parent = objgroup;
    obj(3).Parent = objgroup;

end
