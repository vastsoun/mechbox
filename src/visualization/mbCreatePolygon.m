%
%   obj = mbCreatePolygon (axes,objgroup,points,rgbcolor,transparency,position,orientation)
%
%   Generates and plots a 3D sphere with parametrized characteristics.
%
%   See example "examples/basic_shapes_demo.m" for more info on how to use this function.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function obj = mbCreatePolygon (axes,objgroup,points,rgbcolor,transparency,position,orientation)

    % Create initial transfor group for pose initialization
    polytf = hgtransform('Parent',axes);

    % Initialize point arrays
    X = [];
    Y = [];
    Z = [];
    
    % Initialize point trails
    if ~isempty(points) && isfield(points, 'X')
        X = points.X;
    end
    if ~isempty(points) && isfield(points, 'Y')
        Y = points.Y;
    end
    if ~isempty(points) && isfield(points, 'Z')
        Z = points.Z;
    end
    
    % Create graphics object
    if ~isempty(X) && ~isempty(Y) && ~isempty(Z)
        obj = patch(axes,X,Y,Z,rgbcolor);
    elseif ~isempty(X) && ~isempty(Y)
        obj = patch(axes,X,Y,rgbcolor);
    end
    
    obj.Parent = polytf;
    if ~isempty(transparency)
        obj.FaceAlpha = transparency;
    end
    
    % Initialzie to desired pose
    Txyz = makehgtform('translate', position);
    Rx = makehgtform('xrotate',orientation(1));
    Ry = makehgtform('yrotate',orientation(2));
    Rz = makehgtform('zrotate',orientation(3));
    set(polytf,'Matrix', Rx*Ry*Rz*Txyz);
    drawnow;
    
    % Set object group as the parent object
    if ~isempty(objgroup)
        obj.Parent = objgroup;
    end

end
