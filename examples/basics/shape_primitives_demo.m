%%
%   File:           basic_shapes_demo.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           21/6/2016
%
%   Desciption:     Analysis of a system consisting of a cart, rolling in
%                   1D and attached to spring, while having a hollow 
%                   interior such that a cylinder is free to roll inside. 
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

% Reset MATLAB workspace
close all; clear; clc;

%% Create Scene

% Create figure
scenefig = figure('units','normalized','outerposition',[0 0 1 1]);

% Configure figure properties
scenefig.NumberTitle = 'off';
scenefig.Name = 'MechBox : 3D Objects Demos';

% Create Object Scenes
for j = 1 : 6
    sceneaxes{j} = axes(scenefig);
    subplot(2,3,j,sceneaxes{j});
    set(sceneaxes{j},'TickLabelInterpreter', 'latex','FontSize',12);
    %axis(sceneaxes{j}, 'equal', [-2 2 -2 2 -2 2]);
    axis(sceneaxes{j}, 'equal');
    scenelimits{j} = axis(sceneaxes{j});
    view(3);
    grid on;
    % Configure figure descriptors
    title('Scene','Interpreter','latex','FontSize',16)
    xlabel('x [m]','Interpreter','latex','FontSize',16);
    ylabel('y [m]','Interpreter','latex','FontSize',16);
    zlabel('z [m]','Interpreter','latex','FontSize',16);
    objecttf{j} = hgtransform('Parent',sceneaxes{j});
    camlight;
end

%% Line

line_position = [0 0 0].';
line_orientation = [0 0 0].';
line_length = 2;
line_thickness = 4;
line_color = [0.6 0.6 0.6];
line_transparency = 0.8;
line_handle = mbCreateLine(sceneaxes{1},objecttf{1},line_length,line_thickness,line_color,line_position,line_orientation);

%% Parallelogram

parallelogram_position = [0 0 0].';
parallelogram_orientation = [0 0 0].';
parallelogram_dimensions = [1 1 1];
parallelogram_color = [0.6 0.6 0.6];
parallelogram_transparency = 1.0;
parallelogram_handle = mbCreateParallelogram(sceneaxes{2},objecttf{2},parallelogram_dimensions,parallelogram_color,parallelogram_transparency,parallelogram_position,parallelogram_orientation);

%% Cylinder

cylinder_radius = 1;
cylinder_height = 1;
cylinder_position = [0 0 0].';
cylinder_orientation = [0 0 0].';
cylinder_color = [0.6 0.6 0.6];
cylinder_transparency = 0.9;
cylinder_handle = mbCreateCylinder(sceneaxes{3},objecttf{3},cylinder_radius,cylinder_height,cylinder_color,cylinder_transparency,cylinder_position,cylinder_orientation);

%% Sphere

sphere_radius = 1;
sphere_position = [0 0 0].';
sphere_orientation = [0 0 0].';
sphere_color = [0.6 0.6 0.6];
sphere_transparency = 0.5;
sphere_handle = mbCreateSphere(sceneaxes{4},objecttf{4},sphere_radius,sphere_color,sphere_transparency,sphere_position,sphere_orientation);

%% Spring

spring_length = 4;
spring_radius = 0.2;
spring_windings = 20;
spring_width = 1;
spring_position = [0 0 0].';
spring_orientation = [0 0 0].';
spring_color = [0.6 0.6 0.6];
mbCreateSpring(sceneaxes{5},objecttf{5},spring_length,spring_radius,spring_windings,spring_width,spring_color,spring_position,spring_orientation);

%% Coordinate Frames

csf_position = [0 0 0].';
csf_orientation = [0 0 0].';
[csf_arrows, csf_labels] = mbCreateVisualCoordinateFrame(sceneaxes{6},objecttf{6},'A',csf_position,csf_orientation,1,12);

%% Flat Plane

plane_position = [0 0 0].';
plane_orientation = [0 0 0].';
plane_dimensions = [10 10];
plane_color = [0.95 0.95 0.95];
plane_transparency = 1.0;
plane_handle = mbCreateSurfacePlane(sceneaxes{6},objecttf{6},plane_dimensions,plane_color,plane_transparency,plane_position,plane_orientation);

%%

% EOF
