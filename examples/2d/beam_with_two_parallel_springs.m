%%
%   File:           beam_with_two_parallel_springs.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           8/5/2016
%
%   Desciption:     Analysis of single bar supported vertically by two
%                   springs in a parallel-elastic configuration.
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

 %% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [0 0 0].';

%% Define Bodies

% Define massess
syms m real;

% Define rotational inertias wrt to CoM
syms I real;

% Model Parameters
syms L real;        % Length of bar
syms k real;        % linear stiffness of spring
syms b real;        % Geometric parameter for spring mounting

% Moment of inertia wrt to CoM
I = [0 0 0; 0 0 0; 0 0 (1/12)*m*L^2].';

%% Define Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame
%   {A,i,j,k} - Bar base-frame which rotates by theta long with the bar
%%%

% Cartesian Coordinates
syms x_B y_B theta_B real;  % Point B is where the springs are mounted on the bar
syms x_C y_C theta_C real;  % Point C is the CoG of the bar
syms x_D y_D theta_D real;  % Point D is the tip of the bar where F is applied

% Cartesian Velocities
syms Dx_B Dy_B Dtheta_B real;  % Point B is where the springs are mounted on the bar
syms Dx_C Dy_C Dtheta_C real;  % Point C is the CoG of the bar
syms Dx_D Dy_D Dtheta_D real;  % Point D is the tip of the bar where F is applied

% Cartesian Accelerations
syms DDx_B DDy_B DDtheta_B real;  % Point B is where the springs are mounted on the bar
syms DDx_C DDy_C DDtheta_C real;  % Point C is the CoG of the bar
syms DDx_D DDy_D DDtheta_D real;  % Point D is the tip of the bar where F is applied

%% Absolute Coordinates

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_C y_C theta_C].';
Du = [Dx_C Dy_C Dtheta_C].';
DDu = [DDx_C DDy_C DDtheta_C].';

%% System Constraints

% Holonomic Constraints
f_1 = x_C - (L/2)*cos(theta_C);
f_2 = y_C - (L/2)*sin(theta_C);

% Velocity form of holonomic-constraints
Df_1 = Dx_C + (L/2)*sin(theta_C)*Dtheta_C;
Df_2 = Dy_C - (L/2)*cos(theta_C)*Dtheta_C;

% Non-holonomic Constraints
% NONE

%% Generalized Coordinates

% Generazied vectors
syms q Dq DDq Q real;

% Absolute to generalized coordinate mapping
syms theta real;
syms Dtheta real;
syms DDtheta real;

% Define the generalized quantities
q = theta;
Dq = Dtheta;
DDq = DDtheta;

%% Forward Kinematics 

%%%
% Absolute to Generalized Coordinate Transformations
%%%

%
theta_B = theta;
theta_C = theta;
theta_D = theta;

%
x_B = b*cos(theta);
y_B = b*sin(theta);
x_C = L/2*cos(theta);
y_C = L/2*sin(theta);
x_D = L*cos(theta);
y_D = L*sin(theta);

%
Dtheta_B = Dtheta;
Dtheta_C = Dtheta;
Dtheta_D = Dtheta;

%
Dx_B = -b*sin(theta)*Dtheta;
Dy_B = b*cos(theta)*Dtheta;
Dx_C = -L/2*sin(theta)*Dtheta;
Dy_C = L/2*cos(theta)*Dtheta;
Dx_D = -L*sin(theta)*Dtheta;
Dy_D = L*cos(theta)*Dtheta;

% Absolute position vectors
r_B = [x_B y_B 0].';
r_C = [x_C y_C 0].';
r_D = [x_D y_D 0].';
r_kp = [0 b 0].';   % Static mounting point of Kp
r_kn = [0 -b 0].';  % Static mounting point of Kn
rs_0 = [b 0 0].';   % Coommon mounting point of Kp&Kn on beam

% Absolute velocity vectors
v_B = [Dx_B Dy_B 0].';
v_C = [Dx_C Dy_C 0].';
v_D = [Dx_D Dy_D 0].';

% Angular velocities of CoM 
omega_B = [0 0 Dtheta_B].';
omega_C = [0 0 Dtheta_C].';
omega_D = [0 0 Dtheta_D].';

% Spring deflections from equilibrium positions in absolute coordinates
d_0p = norm(r_kp - rs_0, 2);
d_0n = norm(r_kn - rs_0, 2);

% Define spring deflection in absolute coordinates
defl_kp = norm(r_kp - r_B, 2) - d_0p;
defl_kn = norm(r_kn - r_B, 2) - d_0n;

%% External Forces/Torques

% This system has a single external torque only
syms F_ext real;

% Point-of-Action of the external force F in the A-frame
F_D_inA = [0 F_ext 0].';

% External force F in the O-frame
F_D = mbRotationMatrix('z',theta) * F_D_inA;

% Declare total absolute forces/torques
syms Targs Fargs real;

% Define absolute torques
Targs = [];

% Define absolute forces
Fargs = F_ext;

%% Generalized Forces

% Define the generalized forces from absolute forces acting at point D
syms Q_theta real;

% Define the generalized forces from absolute forces acting at point D
Q_theta = F_D' * jacobian(r_D, q);
Q_theta = simplify(Q_theta, 'Steps', 100);

% Generalized external forces as a function of the absolute external forces
Q_sys([Fargs Targs].') = Q_theta;

%% Kinetics

% Kinetic energy terms
T = (1/2)*m*(v_C.'*v_C) + (1/2)*omega_C.'*I*omega_C;
T = simplify(T, 'Steps', 100);

% Total kinetic energy 
T_sys = T;
T_sys = simplify(T_sys, 'Steps', 100);

% Potential energy terms
V_kp = (1/2)*k*defl_kp^2;
V_kn = (1/2)*k*defl_kn^2;
V_g = m*r_C.'*e_g;

V_kp = simplify(V_kp, 'Steps', 100);
V_kn = simplify(V_kn, 'Steps', 100);
V_g = simplify(V_g, 'Steps', 100);

% Total potential energy 
V_sys = V_kp + V_kn + V_g;
V_sys = simplify(V_sys, 'Steps', 100);

%% System Lagrangian

% System Lagrangian[q, Dq, DDq]
L_sys(t, q.', Dq.') = T_sys - V_sys;
L_sys = simplify(L_sys, 'Steps', 100);

% Display resulting Lagrangian function
display('System Lagrangian: ');
pretty(L_sys == 0);

%% Generate EoM

% Generator configurations
C_sys = [];
eulagconf.Type = 'TimeDependent';

% Compute EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

%% Generate System ODE

% Set which variables represent system parametes
sysparams = [g m L k b].';

% Generator configurations
dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, Dsysvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Numerical Solution

% Set the simulation parameters for the system
siminput = @(t) sin(t);
simparams = [9.81 1.0 1.0 100.0 0.25].';
siminit = [pi/8 0.0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 5.0;
simconf.timeStep = 1e-5;
simconf.solverType = 'ode45';
simconf.notification = false;

% Run the Simulation
[simdata, simtime, solverTime] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Define Simulation Environemnt

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_0 = @(input,params) [(params(3)/2)*cos(input(1)); (params(3)/2)*sin(input(1)); 0.0; 0.0; 0.0; input(1)];

bodies(1).tfs = tf_0;
bodies(1).shapePrimitiveType = 'line';
bodies(1).shapePrimitiveParams = [simparams(3) 2.0];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).name = 'C';
bodies(1).showCS = true;
bodies(1).showTrail = true;

%% Run the Simulation Visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = [-0.2 0.8; -0.5 0.5; -0.5 0.5];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.15;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 20;

% Run the visualization
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% SimulationOverview

simovconf.FontSize = 22;
simovconf.DataSet = [];
simovconf.Print = [];

mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
