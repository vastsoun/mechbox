%%
%   File:           omnidrive_kinematics.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           8/12/2016
%
%   Desciption:     This script demonstrates a derivation of the
%                   non-holonomic kinematic constraints exhibited by
%                   wheeled systems. 
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%%

% Geometric parameters
syms l_1 l_2 R_1 R_2 real;
syms alpha_1 alpha_2 beta_1 beta_2 real;

% Define the offset parameters
pWheelOffsets   = [l_1 l_2].'; 
pWheelRadii     = [R_1 R_2].';
pOffsetAngles   = [alpha_1 alpha_2].';
pSteeringAngles = [beta_1 beta_2].';

% Define the system parameter array
sysparams   = [ pWheelOffsets;
                pWheelRadii;
                pOffsetAngles;
                pSteeringAngles].';
            
% Set the parameter values
simWheelOffsets   = [0.5 0.5].'; 
simWheelRadii     = [0.1 0.1].';
simOffsetAngles   = [-pi/2 pi/2].';
simSteeringAngles = [pi 0].';          
            
% Set the simulation parameters
simparams   = [ simWheelOffsets;
                simWheelRadii;
                simOffsetAngles;
                simSteeringAngles].';

%% Coordinate Systems

% Absolute positions and velocities
syms x_B y_B theta_B real;
syms Dx_B Dy_B Dtheta_B real;

% Velocity of base in local frame
syms Dx_R Dy_R real;

% Wheel joint positions and velocities
syms phi_1 phi_2 real;
syms Dphi_1 Dphi_2 real;

% Define the robot state vectors

xi = [x_B y_B theta_B].';
phi = [phi_1 phi_2].';

Dxi = [Dx_B Dy_B Dtheta_B].';
Dphi = [Dphi_1 Dphi_2].';

%% Generalized Planar Wheeled Robot Equations

%%%
%   For a planar robot with N wheels: 
%
%   [ J_1(beta_s) ] * R_theta * Dxi = [J_2] * Dphi
%   [ C_1(beta_s) ]                   [ 0 ]
%
%   Where:
%
%   1.  Dxi is [ Dx Dy Dtheta ]^T, i.e. the floating-base part of the
%       generalized velocity vector.
%
%   2.  theta is the heading angle of the robot, i.e. the orientation of
%       the B-frame w.r.t the I-frame.
%
%   3.  R_z() is the rotation matrix for rotations about the z-axis
%
%   4.  Dphi is the wheel joint DoFs part of the generalized velocity 
%       vector.
%
%   5.  beta_s is the vector of steering joint angle DoFs part of the
%       generalized coordinate vector.
%
%   6.  J_2 = diag(R_1, R_2, ..., R_n), i.e. the diagonal matrix of the
%       wheel radii.
%
%   7.  J_1 is the rolling-constraints matrix. This is also organized in
%       such a way as to separate the steered and unsteered wheels:
%
%   J_1 = [ J_1f         ] -> the rolling constraints for fixed wheels.
%         [ J_1s(beta_s) ] -> the rolling constraints for steered wheels.
%
%   8.  C_1 is the no-slip constriant matrix. This is also organized in
%       such a way to sparate steered and unsteered wheels:
%
%   C_1 = [ C_1f         ] -> the rolling constraints for fixed wheels.
%         [ C_1s(beta_s) ] -> the rolling constraints for steered wheels.
%
%%%

R_theta = [ cos(theta_B) sin(theta_B) 0;
            -sin(theta_B) cos(theta_B) 0;
            0 0 1];

J_2 = [ R_1 0;
        0 R_2];
    
J_1 = [ sin(alpha_1 + beta_1) -cos(alpha_1 + beta_1) -l_1*cos(beta_1);
        sin(alpha_2 + beta_2) -cos(alpha_2 + beta_2) -l_2*cos(beta_2)];

C_1 = [ cos(alpha_1 + beta_1) sin(alpha_1 + beta_1) 0;
        cos(alpha_2 + beta_2) sin(alpha_2 + beta_2) 0];

J_C = [J_1; C_1];
J_W = [J_2; zeros(2,2)];

%% Forward & Inverse Differential Kinematics 

% We express the forward kinematics Jacobian
J_F = inv(R_theta) * (inv(J_C.'*J_C)*J_C.') * J_W;

% We express the forward kinematics Jacobian
J_I = (inv(J_W.'*J_W)*J_W.') * J_C * R_theta;

% Define functions to compute numerical values
func_J_F = matlabFunction(J_F, 'Vars', {[xi; phi].', sysparams});
func_J_I = matlabFunction(J_I, 'Vars', {[xi; phi].', sysparams});

% Simplified kinematics of differential drive
syms v omega real;
syms r l real;
Dxi_inB = [v 0 omega].';
J_I_simple = simplify(subs(J_I, [alpha_1 alpha_2 beta_1 beta_2 l_1 l_2 R_1 R_2], [-pi/2 pi/2 pi 0 l l r r]));
phi_simple = simplify(J_I_simple*R_theta.'*Dxi_inB);

%% Trajectory Following using Inverse Differential Kinematics

% Initial state of the robot
xi_0 = [1 1 0].';
Dxi_0 = zeros(3,1);

% Inital state of the actuators
Nw = numel(phi);
phi_0 = zeros(Nw,1);
Dphi_0 = zeros(Nw,1);

% Controller mode select
opt_controller_mode = 'polar_kinematic';

% Configure the controller parmeters
K_p = 5;
K_rho = 2;
K_alpha = 5;
K_beta = -0.6;

% Desired trajectory parameters
traj_circle_center = [2 2 0].';
traj_circle_radius = 2;
traj_circle_freq = 0.1;

% Create trajectory generator functions
syms x_C y_C z_C Dx_C Dy_C Dz_C real;
r_C = [x_C y_C z_C].';
v_C = [Dx_C Dy_C Dz_C].';
r_goal = @(t) traj_circle_center + traj_circle_radius*[sin(2*pi*traj_circle_freq*t), cos(2*pi*traj_circle_freq*t), 0.1]';
v_goal = @(t) (2*pi) * (traj_circle_freq * traj_circle_radius) * [cos(2*pi*traj_circle_freq*t),0,-sin(2*pi*traj_circle_freq*t)]';
theta_goal = @(t) 2*pi*traj_circle_freq*t;

% Define here the time resolution
deltaT = 0.01;
time_data = 0:deltaT:(2/traj_circle_freq);

% q, r, and r_goal are stored for every point in time in the following arrays
q_data = zeros(3+Nw+3,length(time_data));
Dq_data = zeros(3+Nw+3,length(time_data));
r_data = zeros(3,length(time_data));
r_goal_data = zeros(3,length(time_data));

% Initialize varables
xi_iter = xi_0;
Dxi_iter = Dxi_0;
phi_iter = phi_0;
Dphi_iter = Dphi_0;

% Run the numerical simulation
for i = 1:length(time_data)
    
    % Update step
    t = time_data(i);
    r_goal_iter = r_goal(t);
    theta_goal_iter = theta_goal(t);
    xi_iter = xi_iter + Dxi_iter*deltaT;
    phi_iter = phi_iter + Dphi_iter*deltaT;
    r_iter = [xi_iter(1) xi_iter(2) 0].';
    
    % Compute current Jacobians from sensor data     
    J_I_iter = func_J_I([xi_iter; phi_iter].', simparams);
    J_F_iter = func_J_F([xi_iter; phi_iter].', simparams);
    
    % Accumulate the new state from the forward kinematics
    Dxi_iter = J_F_iter*Dphi_iter;
    
    % Generate the next target action from the inverse kinematics
    switch opt_controller_mode
        
        case 'position_only' 
            v_iter = K_p*(r_goal_iter - r_iter);
            Dphi_iter = J_I_iter*v_iter;
            
        case 'polar_kinematic'
            % compute control quantities
            rho = sqrt((r_goal_iter(1)-r_iter(1))^2+(r_goal_iter(2)-r_iter(2))^2); 
            lambda = atan2(r_goal_iter(2)-r_iter(2), r_goal_iter(1)-r_iter(1));
            alpha = lambda - xi_iter(3);
            alpha = mod(alpha+pi, 2*pi) - pi;
            beta = - xi_iter(3) - alpha;
            beta = mod(beta+pi, 2*pi) - pi;
            vu = K_rho * rho;
            omega = K_alpha * alpha + K_beta * beta;
            v_iter = [cos(xi_iter(3)) 0; sin(xi_iter(3)) 0; 0 1] * [vu; omega];
            Dphi_iter = J_I_iter*v_iter;
            
        otherwise
            error('Error: Incorrect controller mode has been specified.');
    end
    
    
    % Logging
    r_data(:,i) = r_iter;
    r_goal_data(:,i) = r_goal_iter;
    q_data(:,i) = [xi_iter; phi_iter; r_goal_iter];
    Dq_data(:,i) = [Dxi_iter; Dphi_iter; [0 0 0].'];
end

%% Kinematics for the Visualization

r_IB_inI = [xi(1) xi(2) 0].';
r_W1_inW1 = [1 0 0].';
r_W2_inW2 = [1 0 0].';
r_BW1_inW1 = [-l_1 0 0.1].';
r_BW2_inW2 = [l_2 0 0.1].';

R_IB = [ cos(theta_B) -sin(theta_B) 0;
         sin(theta_B)  cos(theta_B) 0;
         0 0 1];

R_BW1 = [ cos(alpha_1+beta_1) -sin(alpha_1+beta_1) 0;
          sin(alpha_1+beta_1)  cos(alpha_1+beta_1) 0;
          0 0 1];

R_BW2 = [ cos(alpha_2+beta_2) -sin(alpha_2+beta_2) 0;
          sin(alpha_2+beta_2)  cos(alpha_2+beta_2) 0;
          0 0 1];
      
R_W1 = [ 1 0 0;
         0 cos(phi_1) sin(phi_1);
         0 -sin(phi_1) cos(phi_1)];

R_W2 = [ 1 0 0;
         0 cos(phi_2) sin(phi_2);
         0 -sin(phi_2) cos(phi_2)];
    
% Frame and body positions
r_IW1_inI = r_IB_inI + R_IB*R_BW1*r_BW1_inW1;
r_IW2_inI = r_IB_inI + R_IB*R_BW2*r_BW2_inW2; 
r_W1_inI = simplify(R_IB*R_BW1*r_W1_inW1);
r_W2_inI = simplify(R_IB*R_BW2*r_W2_inW2); 

% Wheel orientations
theta_W1_x = 0;
theta_W1_y = 0;
theta_W1_z = atan2(r_W1_inI(2),r_W1_inI(1));
theta_W2_x = 0;
theta_W2_y = 0;
theta_W2_z = atan2(r_W2_inI(2),r_W2_inI(1));

% Compute the total rotation matrices, interpreted as cosine-matrices
C_W1 = R_IB*R_BW1*R_W1.';
C_W2 = R_IB*R_BW2*R_W2.'; 
r_WF1_inI = simplify(C_W1*r_W1_inW1);
r_WF2_inI = simplify(C_W2*r_W2_inW2); 

% Compute angle-axis
[u_W1, theta_W1] = mbRotationMatrixToAxisAngle(C_W1);
[u_W2, theta_W2] = mbRotationMatrixToAxisAngle(C_W2);

% Poses
pose_B_inI  = [[xi(1) xi(2) 0] [0 0 1 xi(3)]].';
pose_W1_inI = [r_IW1_inI.' u_W1.' theta_W1].';
pose_W2_inI = [r_IW2_inI.' u_W2.' theta_W2].';
pose_WF1_inI = [r_IW1_inI.' u_W1.' theta_W1].';
pose_WF2_inI = [r_IW2_inI.' u_W2.' theta_W2].';

% 
% theta_WF1_x = 0;
% theta_WF1_y = 0;
% theta_WF1_z = atan2(r_WF1_inI(2),r_WF1_inI(1));
% theta_WF2_x = 0;
% theta_WF2_y = 0;
% theta_WF2_z = atan2(r_WF2_inI(2),r_WF2_inI(1));
% pose_B_inI  = [[xi(1) xi(2) 0] [0 0 1 xi(3)]].';
% pose_W1_inI = [r_IW1_inI.' theta_W1_x theta_W1_y theta_W1_z].';
% pose_W2_inI = [r_IW2_inI.' theta_W2_x theta_W2_y theta_W2_z].';
% pose_WF1_inI = [r_IW1_inI.' theta_WF1_x theta_WF1_y theta_WF1_z].';
% pose_WF2_inI = [r_IW2_inI.' theta_WF2_x theta_WF2_y theta_WF2_z].';

%% Multi-Rigid-Body Kinematics Generator Functions

tf_B    = matlabFunction(pose_B_inI, 'Vars', {[xi; phi; Dxi; Dphi; r_C; v_C].', sysparams});
tf_W1   = matlabFunction(pose_W1_inI, 'Vars', {[xi; phi; Dxi; Dphi; r_C; v_C].', sysparams});
tf_W2   = matlabFunction(pose_W2_inI, 'Vars', {[xi; phi; Dxi; Dphi; r_C; v_C].', sysparams});
tf_WF1   = matlabFunction(pose_WF1_inI, 'Vars', {[xi; phi; Dxi; Dphi; r_C; v_C].', sysparams});
tf_WF2   = matlabFunction(pose_WF2_inI, 'Vars', {[xi; phi; Dxi; Dphi; r_C; v_C].', sysparams});
tf_C    = @(data,params) [data(6) data(7) data(8) 0 0 1 0].';

%% Define Visualization Environment

% Base
bodies(1).tfs = tf_B;
bodies(1).shapePrimitiveType = 'cylinder';
bodies(1).shapePrimitiveParams = [0.45 0.1];
bodies(1).shapeOffsets = [0 0 0.1 0 0 0].';
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).showCS = true;
bodies(1).showTrail = true;
bodies(1).name = 'B';

% Wheels
bodies(2).tfs = tf_W1;
bodies(2).shapePrimitiveType = 'cylinder';
bodies(2).shapePrimitiveParams = [0.1 0.05];
bodies(2).shapeColor = [0.8 0.3 0.3 0.3];
bodies(2).shapeOffsets = [0 0 0 0 pi/2 0].';
bodies(2).showCS = false;
bodies(2).showTrail = false;
bodies(2).name = 'W1';

bodies(3).tfs = tf_W2;
bodies(3).shapePrimitiveType = 'cylinder';
bodies(3).shapePrimitiveParams = [0.1 0.05];
bodies(3).shapeColor = [0.8 0.3 0.3 0.3];
bodies(3).shapeOffsets = [0 0 0 0 pi/2 0].';
bodies(3).showCS = false;
bodies(3).showTrail = false;
bodies(3).name = 'W2';

% Wheel Frames
bodies(4).tfs = tf_WF1;
bodies(4).shapePrimitiveType = 'none';
bodies(4).showCS = true;
bodies(4).showTrail = false;
bodies(4).name = 'W1';

bodies(5).tfs = tf_WF2;
bodies(5).shapePrimitiveType = 'none';
bodies(5).showCS = true;
bodies(5).showTrail = false;
bodies(5).name = 'W2';

% The target frame
bodies(6).tfs = tf_C;
bodies(6).shapePrimitiveType = 'none'; 
bodies(6).showCS = true;
bodies(6).showTrail = true;
bodies(6).name = 'C';

%% Run the Simulation Visualization

% Configure visualization data
simdata = [q_data.' Dq_data.'].';
simtime = time_data;

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = [-1 5; -1 5; -0.5 2.5];
simvizconf.fps = 20.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.3;
simvizconf.csfontsize = 18;
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 18;

% Run the visualizers
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% Simulation Overivew

simovconf.FontSize = 18;
simovconf.DataSet = [4 5; 9 10];
mbSimulationOverview(simtime,simdata,[xi; phi; Dxi; Dphi].',simovconf);

%% 
% EOF
