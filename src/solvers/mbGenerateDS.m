%
%   function [sysf, vars, Dvars] = mbGenerateDS(equ, params, t, q, Dq, DDq, F, Lambda, conf)
%
%   This function generates a dynamical system representation from a set of
%   EoM, which can then be solved using numerical solvers.
%
%   inputs:
%       equ     Symbolic expression of the system equations
%       params  Vector of symbolic variables representing system parameters
%       t       Symbolic variable for time
%       q       Symbolic vector of generalized coordinates
%       F       Vector of parametrized external forces
%       Lambda  Vector of Lagrangian multipliers for constrained systems
%
%   returns:
%       sysf    Function handle to the system dynamics generator
%       vars    Vector of symbolic state-variables for the generated system
%       Dvars   Vector of state variables expressed as functions of 
%               derivatives of other state variables.
%
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function [sysf, vars, Dvars] = mbGenerateDS(equ, params, t, q, Dq, DDq, F, Lambda, conf)

    %
    if ~isempty(conf) && isfield(conf, 'Type')
        switch conf.Type
            case 'TimeDependent'
                [sysf, vars, Dvars] = mbGenerateDSTimeDep(equ, params, t, q, F, Lambda, conf);
            case 'TimeIndependent'
                [sysf, vars, Dvars] = mbGenerateDSTimeIndep(equ, params, t, q, Dq, DDq, F, Lambda, conf);
            otherwise
                error('error: mbGenerateDS: Incorrect "systemType" has been specified.');
        end
    end

end
