%%
%   File:           two_masses_two_springs_sliding_on_bar.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           7/6/2016
%
%   Desciption:     Analysis of a system with two masses coupled by springs
%                   sliding on a rotating bar.
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [-g 0 0].';

%% Define Bodies

% Define massess
syms m_0 m_1 m_2 real;

% Define rotational inertias wrt to CoM
syms I_0 I_1 I_2 real;

% Model Parameters
syms k L_0 L_1 L_2;

% Moment of inertia wrt to CoM
I_0 = [0 0 0; 0 0 0; 0 0 (1/12)*m_0*L_0^2].';
I_1 = [0 0 0; 0 0 0; 0 0 0].';
I_2 = [0 0 0; 0 0 0; 0 0 0].';

%% Cartesian Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame.
%   {P_n,i_n,j_n,k_n} - Local frame at point P_n. 
%%%

% Define absolute coodrinates
syms x_0 y_0 theta_0 real; % CoM of bar m_0
syms x_1 y_1 theta_1 real; % CoM of slider m_1
syms x_2 y_2 theta_2 real; % CoM of slider m_2

% Define absolute velocities
syms Dx_0 Dy_0 Dtheta_0 real; 
syms Dx_1 Dy_1 Dtheta_1 real; 
syms Dx_2 Dy_2 Dtheta_2 real; 

% Define absolute accelerations
syms DDx_0 DDy_0 DDtheta_0 real; 
syms DDx_1 DDy_1 DDtheta_1 real; 
syms DDx_2 DDy_2 DDtheta_2 real; 

%% System Constraints

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_0 y_0 theta_0 x_1 y_1 theta_1 x_2 y_2 theta_2].';
Du = [Dx_0 Dy_0 Dtheta_0 Dx_1 Dy_1 Dtheta_1 Dx_2 Dy_2 Dtheta_2].';
DDu = [DDx_0 DDy_0 DDtheta_0 DDx_1 DDy_1 DDtheta_1 DDx_2 DDy_2 DDtheta_2].';

% Holonomic Constraints
f_1(u.') = theta_1 - theta_0 == 0;
f_2(u.') = theta_2 - theta_0 == 0;
f_3(u.') = x_0*tan(theta_0) - y_0 == 0;
f_4(u.') = x_1*tan(theta_1) - y_1 == 0;
f_5(u.') = x_2*tan(theta_2) - y_2 == 0;
f_6(u.') = x_0^2 + y_0^2 - (L_0/2)^2 == 0;

%
%surf();

% Velocity form of holonomic-constraints
Df_1(u.', Du.') = Dtheta_1 - Dtheta_0 == 0;
Df_2(u.', Du.') = Dtheta_2 - Dtheta_0 == 0;
Df_3(u.', Du.') = Dx_0*tan(theta_0) + x_0*Dtheta_0*((cos(theta_0))^(-2)) - Dy_0 == 0;
Df_4(u.', Du.') = Dx_1*tan(theta_1) + x_1*Dtheta_1*((cos(theta_1))^(-2)) - Dy_1 == 0;
Df_5(u.', Du.') = Dx_2*tan(theta_2) + x_2*Dtheta_2*((cos(theta_2))^(-2)) - Dy_2 == 0;
Df_6(u.', Du.') = x_0*Dx_0 + y_0*Dy_0 == 0;

% Non-holonomic Constraints
% NONE

%% Generalized Coordinates

% Generalized displacements
syms theta R_1 R_2 real;

% Generalized velocities
syms Dtheta DR_1 DR_2 real;

% Generalized accelerations
syms DDtheta DDR_1 DDR_2 real;

%% Forward Kinematics 

%
theta_0 = theta;
theta_1 = theta;
theta_2 = theta;

%
x_0 = (L_0/2)*cos(theta_0);
y_0 = (L_0/2)*sin(theta_0);
x_1 = (R_1)*cos(theta_1);
y_1 = (R_1)*sin(theta_1);
x_2 = (R_1 + R_2)*cos(theta_2);
y_2 = (R_1 + R_2)*sin(theta_2);

%
Dtheta_0 = Dtheta;
Dtheta_1 = Dtheta;
Dtheta_2 = Dtheta;

%
Dx_0 = - (L_0/2)*Dtheta_0*sin(theta_0);
Dy_0 =   (L_0/2)*Dtheta_0*cos(theta_0);
Dx_1 = DR_1*cos(theta_1) - R_1*Dtheta_1*sin(theta_1);
Dy_1 = DR_1*sin(theta_1) + R_1*Dtheta_1*cos(theta_1);
Dx_2 = (DR_1+DR_2)*cos(theta_2) - (R_1+R_2)*Dtheta_2*sin(theta_2);
Dy_2 = (DR_1+DR_2)*sin(theta_2) + (R_1+R_2)*Dtheta_2*cos(theta_2);

% Absolute position vectors
r_0 = [x_0 y_0 0].';
r_1 = [x_1 y_1 0].';
r_2 = [x_2 y_2 0].';

% Absolute velocity vectors
v_0 = [Dx_0 Dy_0 0].';
v_1 = [Dx_1 Dy_1 0].';
v_2 = [Dx_2 Dy_2 0].';

% Angular velocities of CoM 
omega_0 = [0 0 Dtheta_0].';
omega_1 = [0 0 Dtheta_1].';
omega_2 = [0 0 Dtheta_2].';

%% Constraint Forces/Torques

%
% TODO
%

%% External Forces/Torques

% This system has a single external torque only
syms T_ext real;

%
syms T F real;

% Define absolute forces
T = T_ext;

% Define absolute torques
F = [];

%% Generalized Vectors

% Generazied quantities
syms q Dq DDq Q real;

% Define the generalized quantities
q = [theta R_1 R_2].';
Dq = [Dtheta DR_1 DR_2].';
DDq = [DDtheta DDR_1 DDR_2].';

% Define the generalized forces from absolute forces acting at point D
Q_theta = T_ext;
Q_R_1 = 0;
Q_R_2 = 0;

% Generalized external forces as a function of the absolute external forces
Q_sys([F T].') = [Q_theta Q_R_1 Q_R_2].';

%% Kinetics

% Kinetic energy terms
T_0 = (1/2)*m_0*(v_0.'*v_0) + (1/2)*omega_0.'*I_0*omega_0;
T_1 = (1/2)*m_1*(v_1.'*v_1) + (1/2)*omega_1.'*I_1*omega_1;
T_2 = (1/2)*m_2*(v_2.'*v_2) + (1/2)*omega_2.'*I_2*omega_2;

T_0 = simplify(T_0, 'Steps', 100);
T_1 = simplify(T_1, 'Steps', 100);
T_2 = simplify(T_2, 'Steps', 100);

% Total kinetic energy 
Tsys = T_0 + T_1 + T_2;
Tsys = simplify(Tsys, 'Steps', 100);

% Potential energy terms
V_k1 = (1/2)*k*(R_1 - L_1)^2;
V_k2 = (1/2)*k*(R_2 - L_2)^2;
V_g0 = m_0*r_0.'*e_g;
V_g1 = m_1*r_1.'*e_g;
V_g2 = m_2*r_2.'*e_g;

V_k1 = simplify(V_k1, 'Steps', 100);
V_k2 = simplify(V_k2, 'Steps', 100);
V_g0 = simplify(V_g0, 'Steps', 100);
V_g1 = simplify(V_g1, 'Steps', 100);
V_g2 = simplify(V_g2, 'Steps', 100);

% Total potential energy 
Vsys = V_k1 + V_k2 + V_g0 + V_g1 + V_g2;
Vsys = simplify(Vsys, 'Steps', 100);

%% System Lagrangian

% System Lagrangian[q, Dq, DDq]
L_sys(t, q.', Dq.') = Tsys - Vsys;
L_sys = simplify(L_sys, 'Steps', 100);

% Display resulting Lagrangian function
display('System Lagrangian: ');
pretty(L_sys == 0);

%% Generate EoM

% Generator configurations
C_sys = [];
eulagconf.Type = 'TimeDependent';

% Compute EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

% Display resulting EoM
display('Equations of Motion: ');
pretty(EoM);

%% Generate ODE System

% Set which variables represent system parametes
sysparams = [g m_0 m_1 m_2 L_0 L_1 L_2 k].';

% Generator configurations
dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, Dsysvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Numerical Solutions

% Configure the simulation time paremeters
Tf = 10.0;
Ts = 1e-5;

% Set the simulation parameters for the system
%siminput = @(t) sin(t);
siminput = @(t) 0.0;
simparams = [9.81 0.1 0.2 0.2 1.0 0.1 0.1 100.0].';
siminit = [pi/8 0.1 0.1 0.0 0.0 0.0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 20.0;
simconf.timeStep = 1e-3;
simconf.solverType = 'ode45';
simconf.notification = true;

% Run the Simulation
[simdata, simtime, solverTime] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Define Simulation Environment

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_0 = @(input,params) [params(5)*cos(input(1)); params(5)*sin(input(1)); 0.0; 0.0; 0.0; input(1)];
tf_1 = @(input,params) [input(2)*cos(input(1)); input(2)*sin(input(1)); 0.0; 0.0; 0.0; input(1)];
tf_2 = @(input,params) [(input(2)+input(3))*cos(input(1)); (input(2)+input(3))*sin(input(1)); 0.0; 0.0; 0.0; input(1)];

bodies(1).tfs = tf_0;
bodies(1).shapePrimitiveType = 'line';
bodies(1).shapePrimitiveParams = [simparams(5)*2 2];
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).name = '0';
bodies(1).showCS = true;
bodies(1).showTrail = true;

bodies(2).tfs = tf_1;
bodies(2).shapePrimitiveType = 'parallelogram';
bodies(2).shapePrimitiveParams = [0.06 0.06 0.06];
bodies(2).shapeColor = [0.6 0.6 0.6 0.6]; 
bodies(2).name = '1';
bodies(2).showCS = true;
bodies(2).showTrail = true;

bodies(3).tfs = tf_2;
bodies(3).shapePrimitiveType = 'parallelogram';
bodies(3).shapePrimitiveParams = [0.06 0.06 0.06];
bodies(3).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(3).name = '2';
bodies(3).showCS = true;
bodies(3).showTrail = true;

%% Run the Simulation Visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = [-0.2 1.8; -1 1; -1 1];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.15;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 20;

% Run the visualization
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% SimulationOverview

simovconf.FontSize = 22;
simovconf.DataSet = [];
simovconf.Print = [];

mbSimulationOverview(simtime,simdata,[q Dq],simovconf);

%% 
% EOF
