%%
%   File:           compliant_wheel_tyre_model.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           3/7/2016
%
%   Desciption:     Analysis of the kinematics and dynamics of an elastic 
%                   tyre-ground contact model.
%
%
%
%   Copyright(C) 2016, Vassilios Tsounis.
%   

close all; clear; clc;

% Set consolde output format
format short e;

%% Model Configurations

opt_use_traction_model = false;
opt_apply_dissipative_forces = true;

%% Define Model Variables & Parameters

% Kinematic Parameters
syms  R_t R_w  d_x d_y;
assume(R_t>0 & R_w>0);

% Dynamic parameters
syms k_wt k_tc mu_c beta_t real
assume(k_wt>0 & k_tc>0 & mu_c>0 & beta_t>0);

% Define system coordinate variables
syms x_w y_w theta_w real;
syms x_t y_t theta_t real;
syms x_A y_A real;
syms x_B y_B real;
syms x_C y_C real;
syms x_D y_D real;
syms x_c y_c real;
syms phi phi_w phi_t dphi_w dphi_t dphi real;

%% Define Kinematic/Geometric Relationships 

% Cartesian position of the bodies in I-frame
r_w = [x_w y_w 0].';
r_t = [x_t y_t 0].';

% Cartesian position of the contact point in I-frame
x_c = x_t;
r_c = [x_c 0 0].';

% Cartesian position of the infitesimal spring attachment points
r_A = r_t + mbRotationMatrix('z',theta_t)*[R_t*cos(phi_t) R_t*sin(phi_t) 0].';
r_B = r_w + mbRotationMatrix('z',theta_w)*[R_w*cos(phi_w) R_w*sin(phi_w) 0].';
r_A = simplify(r_A);
r_B = simplify(r_B);

% mbSymbolic2Latex([],[],r_w,18,'plot');
% mbSymbolic2Latex([],[],r_t,18,'plot');
% mbSymbolic2Latex([],[],r_A,18,'plot');
% mbSymbolic2Latex([],[],r_B,18,'plot');

%% Wheel-Tyre Compliant Contact Model

% Relative vector between the two ends of the infinitesimal spring element
r_dUwt = r_A - r_B;
%mbSymbolic2Latex([],'\vec{\bf{r}}_{dU_{wt}} = ',r_dUwt,18,'plot');

% Length of the infinitesimal spring element
L2_wt = simplify(norm(r_dUwt)^2);
%mbSymbolic2Latex([],'L^2_{dU_{wt}} = ',L2_wt,18,'plot');

%
%dU_wt = (1/2)*k_wt*L2_wt*dphi;
%mbSymbolic2Latex([],'dU_{wt} = ',dU_wt,18,'plot');

% Define the infinitesimal spring potential
phi_w = phi;
phi_t = phi;
L2_wt = subs(L2_wt);
dU_wt = (1/2)*k_wt*L2_wt;
%mbSymbolic2Latex([],'dU_{wt} = ',dU_wt,18,'plot');

% Integrate to generate the total potential energy for the wheel-tyre
% system
U_wt = int(dU_wt,phi,[0 2*pi]);
U_wt = simplify(U_wt);
%mbSymbolic2Latex([],'U_{wt} = ',U_wt,18,'plot');

%% Generate matlab functions from the kinematics/geometric relationships

%
func_r_w = matlabFunction(r_w);
func_r_t = matlabFunction(r_t);

%
func_r_A = matlabFunction(r_A);
func_r_B = matlabFunction(r_B);

%
func_r_c = matlabFunction(r_c);

%% Visualize the Compliant Tyre-Wheel Model

% Create visualization figure
%scenefigure = figure;
scenefigure = figure('units','normalized','outerposition',[0 0 1 1]);
scenefigure.NumberTitle = 'off';
scenefigure.Name = 'fsrc2016 : Wheel-Tyre Elastic Model';
sceneaxes = axes(scenefigure);
%subplot(3,4,[1:3 5:7 9:11], sceneaxes);
set(sceneaxes,'TickLabelInterpreter', 'latex','FontSize',12);
view(2);
axis(sceneaxes, 'equal');
%grid on;
%camlight;
csscale = 0.4;
csfont = 18;

set(sceneaxes,'xtick',[])
set(sceneaxes,'xticklabel',[])
set(sceneaxes,'ytick',[])
set(sceneaxes,'yticklabel',[])
set(sceneaxes, 'visible', 'off') ;

%
for j = 1:5
    objecttf{j} = hgtransform('Parent',sceneaxes);
end

mbCreateVisualCoordinateFrame(sceneaxes,objecttf{5},'I',[0 0 0].',[0 0 0].',csscale,csfont);

%
Rw = 1;
Rt = 2;
xw = 2.43; 
yw = 2.12;
xt = 2; 
yt = 2;
thetaw = pi/1.1;
thetat = pi/1.3;
phiw = 0;
phit = 0;

%
pos_I = [0 0 0].';
orient_null = [0 0 0].';
pos_w = func_r_w(xw,yw);
pos_t = func_r_t(xt,yt);
pos_A = func_r_A(Rt,phit,thetat,xt,yt);
pos_B = func_r_B(Rw,phiw,thetaw,xw,yw);
pos_c = func_r_c(xt);

%
orient_w = [0 0 thetaw].'; 
orient_t = [0 0 thetat].';
orient_A = [0 0 0].';
orient_B = [0 0 0].';

%
mbCreateCylinder(sceneaxes,objecttf{1},Rw,0.6,[0.7 0.7 0.7],0.6,pos_I,orient_null);
mbCreateVisualCoordinateFrame(sceneaxes,objecttf{1},'w',[0;0;0.8],pos_I,csscale,csfont);
Txyz = makehgtform('translate', pos_w);
Rx = makehgtform('xrotate',orient_w(1));
Ry = makehgtform('yrotate',orient_w(2));
Rz = makehgtform('zrotate',orient_w(3));
set(objecttf{1},'Matrix', Txyz*Rx*Ry*Rz);
drawnow;

%
mbCreateCylinder(sceneaxes,objecttf{2},Rt,0.5,[0.3 0.3 0.3],0.5,pos_I,orient_null);
mbCreateVisualCoordinateFrame(sceneaxes,objecttf{2},'t',[0;0;0.8],pos_I,csscale,csfont);
Txyz = makehgtform('translate', pos_t);
Rx = makehgtform('xrotate',orient_t(1));
Ry = makehgtform('yrotate',orient_t(2));
Rz = makehgtform('zrotate',orient_t(3));
set(objecttf{2},'Matrix', Txyz*Rx*Ry*Rz);
drawnow;

%
posv_I = [pos_I(1:2).' 0.6].';
posv_w = [pos_w(1:2).' 0.6].';
posv_t = [pos_t(1:2).' 0.6].';
posv_A = [pos_A(1:2).' 0.6].';
posv_B = [pos_B(1:2).' 0.6].';
posv_c = [pos_c(1:2).' 0.6].';

%
mbCreateVectorArrow(sceneaxes,objecttf{5},'wt','cyan',1.5,posv_w,posv_t,csscale,20);
mbCreateVectorArrow(sceneaxes,objecttf{5},'A','magenta',1.5,posv_I,posv_A,csscale,20);
mbCreateVectorArrow(sceneaxes,objecttf{5},'B','magenta',1.5,posv_I,posv_B,csscale,20);
mbCreateVectorArrow(sceneaxes,objecttf{5},'dU_{s}','cyan',1.5,posv_A,posv_B,csscale,20);
mbCreateVectorArrow(sceneaxes,objecttf{5},'c','magenta',1.5,posv_I,posv_c,csscale,20);
mbCreateVectorArrow(sceneaxes,objecttf{5},'ct','cyan',1.5,posv_c,posv_t,csscale,20);

%% World Time

% Define Time Variable
syms t real;

%% World Gravity

% Acceleration of gravity in m/s^2
syms g real;

% Vector of gravitational potential field in inertial world-frame
e_g = [0 g 0].';

%% Define Bodies

% Define massess
syms m_w m_t real;
assume(m_w>0 & m_t>0);

% Moment of inertia wrt to CoM
I_w = [0 0 0; 0 0 0; 0 0 (1/2)*m_w*R_w^2].';
I_t = [0 0 0; 0 0 0; 0 0 (1/2)*m_t*R_t^2].';

%% Define Coordinates

%%%
%   We define the following reference frames:
%
%   {O,x,y,z} - Global inertial frame.
%   {P_n,i_n,j_n,k_n} - Local frame at point P_n. 
%%%

% Define absolute velocities
syms Dx_w Dy_w Dtheta_w real; 
syms Dx_t Dy_t Dtheta_t real; 

% Define absolute accelerations
syms DDx_w DDy_w DDtheta_w real; 
syms DDx_t DDy_t DDtheta_t real; 

%% Absolute Coordinates

% Declare absolute displacement &  velocity vectors
syms u Du DDu real;

% Define absolute displacement & velocity vectors
u = [x_w y_w theta_w x_t y_t theta_t].';
Du = [Dx_w Dy_w Dtheta_w Dx_t Dy_t Dtheta_t].';
DDu = [DDx_w DDy_w DDtheta_w DDx_t DDy_t DDtheta_t].';

%% Generalized Coordinates

% Generazied quantities
syms q Dq real;

% Define the generalized quantities
q = u;
Dq = Du;
DDq = DDu;

%% Forward Velocity Kinematics

% Absolute velocity vectors
v_w = [Dx_w Dy_w 0].';
v_t = [Dx_t Dy_t 0].';

% Angular velocities of CoM 
omega_w = [0 0 Dtheta_w].';
omega_t = [0 0 Dtheta_t].';

%% Tyre-Ground Contact Model

%%%
%   NOTE:
%
%   We will define the elastic contact force as well as the potential
%   energy of the elastic element. This allows us to either incorporate the
%   dynamics of the contact either through the contact force or either via
%   the potential energy.
%
%%%

% Vector from tyre to contact
r_ct = r_c - r_t;

% Define the cartesian force in the y direction
F_ct = - k_tc*(R_t - y_t)*heaviside(R_t - y_t);

% Potential energy of the elastic element
U_ktc = int(F_ct,y_t);

%% Dissipative Forces

%%%
%   NOTE:
%
%   We choose not to use Rayleigh dissipation functions, but rather to
%   incorporate the dissipative forces as external non-conservative forces.
%
%%%

% Define the tyre-ground damping force
F_dt = Dy_t*beta_t*heaviside(R_t - y_t);

% Generalized force
Q_dis = jacobian(r_ct, q).'*[0 F_dt 0].';

%% A Simple Model for Wheel Traction Force

% A simple traction model
F_tr = heaviside(R_t - y_t)*(sign(Dtheta_t)*((m_w+m_t)*g + (jacobian(U_ktc,y_t)))*mu_c + beta_t*Dtheta_t);
%mbSymbolic2Latex([],'\vec{\bf{F}}_{tr} = ',F_tr,18,'plot');

% Generalized force due to the Traction force/friction.
Q_trac = jacobian(r_c, q).' * [F_tr 0 0].';
%mbSymbolic2Latex([],'\bf{Q}_{tr} = ',Q_tr,18,'plot');

%% Kinetic Energy

% Kinetic energy terms
T_w = (1/2)*m_w*(v_w.'*v_w) + (1/2)*omega_w.'*I_w*omega_w;
T_t = (1/2)*m_t*(v_t.'*v_t) + (1/2)*omega_t.'*I_t*omega_t;

T_w = simplify(T_w);
T_t = simplify(T_t);

% Total kinetic energy 
Tsys = T_w + T_t;
Tsys = simplify(Tsys);
%mbSymbolic2Latex([],'\mathcal{T}_{sys} = ',Tsys,18,'plot');

%% Potential Energy

% Gravitational Potentials
V_gw = m_w*r_w.'*e_g;
V_gt = m_t*r_t.'*e_g;

% Wheel-Tyre Elastic Potentials
V_kwt = U_wt;

% Tyre-Ground Elastic Potentials
V_ktc = U_ktc;

% Total potential energy 
Vsys = V_gw + V_gt + V_kwt + V_ktc;
Vsys = simplify(Vsys);
%mbSymbolic2Latex([],'\mathcal{V}_{sys} = ',Vsys,18,'plot');

%% System Lagrangian

% System Lagrangian(q, Dq)
L_sys(t, q.', Dq.') = Tsys - Vsys;
L_sys = simplify(L_sys);

% Display resulting Lagrangian functionuse_traction_model
display('System Lagrangian: ');
%pretty(L_sys == 0);

% Generate string for the Lagrangian's independent variables
largs = '';
for i = 1:numel(2*q)
    if i <= numel(q)
        varchar = mbSymbolic2Latex([],[],q(i),[],'raw');
        largs = strcat(largs,varchar,',');
    elseif i < numel(2*q)
        varchar = mbSymbolic2Latex([],[],Dq(i),[],'raw');
        largs = strcat(largs,varchar,',');
    elseif i == numel(2*q)
        varchar = mbSymbolic2Latex([],[],Dq(i),[],'raw');
        largs = strcat(largs,varchar);
    end
end

ltitle = strcat('\mathcal{L}_{sys}(',largs,') = ');
%latex_lagrangian = mbSymbolic2Latex([],ltitle,L_sys,18,'plot');

%% Definition of External Forces

% Define the exernal force in the dimensions of the absolute linear
% displacements
Fargs = [];

% Define the exernal torques in the dimensions of the absolute angular
% displacements
Targs = [];

%% External Generalized Forces/Torques

% We allocate variables for future experimentation
syms Q_x_w Q_y_w Q_theta_w Q_x_t Q_y_t Q_theta_t real;

% Initialize to zero
Q_x_w = 0;
Q_y_w = 0;
Q_theta_w = 0;
Q_x_t = 0;
Q_y_t = 0;
Q_theta_t = 0;

% Generalized forces
Qdef = [Q_x_w Q_y_w Q_theta_w Q_x_t Q_y_t Q_theta_t].';

%% Component Generalized Forces

% Apply the contact force
%Qdef = Qdef + Q_c;

% Configure the model to use(or not) the dissipative forces
if opt_apply_dissipative_forces == true
    Qdef = Qdef - Q_dis;
end

% Configure the model to use(or not) the traction forces
if opt_use_traction_model == true
    Qdef = Qdef - Q_trac;
end

%% Total Generalized Forces

% Generalized external forces as a function of external forces
if ~isempty(Fargs) && ~isempty(Targs)
    Q_sys([Fargs Targs].') = Qdef;
else
    Q_sys = Qdef;
end

%% Generate EoM

% Configurations
C_sys = [];
eulagconf.Type = 'TimeIndependent';

% Calculate EoM using Euler-Lagrange equations
[EoM, qt, Dqt, DDqt, Qt, Ft, Lambdat] = mbEulerLagrange(t, q, Dq, DDq, L_sys, Q_sys, C_sys, eulagconf);

%% Define System Parameters

% Set which variables represent system parameters
sysparams = [g m_w m_t R_t R_w k_wt k_tc mu_c beta_t].';

%% Generate Dynamical System from EoM

dsgenconf.Output = '';
dsgenconf.Type = eulagconf.Type;

% Generate the ODE system function which will generate the system dynamics
[syseq, sysvars, Dsysvars] = mbGenerateDS(EoM, sysparams, t, qt, Dqt, DDqt, Ft, Lambdat, dsgenconf);

%% Execution of Numerical Solution

% Set the parameters, additional external inputs, and initial conditions
siminput = [];
simparams = [9.81 1.0 1.0 0.5 0.3 2.5e4 5e5 0.1 10.0].';
%siminit = [0 1 0 0 1 0 0 0 -1.5 0 0 -1.5].';
siminit = [0 1 0 0 1 0 0 0 0 0 0 0].';

% Set the simulation configurations
simconf.timeStart = 0.0;
simconf.timeStop = 10.0;
simconf.timeStep = 1e-3;
simconf.solverType = 'ode45';
simconf.notification = true;

% Run the generic dynamical-system (DS) solver
[simresult, simtime, simperf] =  mbSimulateDS(syseq, simparams, siminput, siminit, simconf);

%% Forward kinematic transformations

% Set the transformation function which produces cartesian trajectories
% from system trajectories
tf_w = @(input,params) [input(1); input(2); 0; 0; 0; input(3)];
tf_t = @(input,params) [input(4); input(5); 0; 0; 0; input(6)];

%% Define Simulation Environment

% define a flat surface to visualize na ideal planar ground
bodies(1).tfs = @(input,params) [-5;0;-5;pi/2;0;0];
bodies(1).shapePrimitiveType = 'plane';
bodies(1).shapePrimitiveParams = [5 5 0];
bodies(1).shapeColor = [0.4 0.95 0.95 0.95]; 
bodies(1).showCS = false;
bodies(1).showTrail = false;
bodies(1).name = 'f';

% define the wheel object primitve
bodies(2).tfs = tf_w;
bodies(2).shapePrimitiveType = 'cylinder';
bodies(2).shapePrimitiveParams = [simparams(5) 0.6];
bodies(2).shapeColor = [0.5 0.7 0.7 0.7];
bodies(2).showCS = true;
bodies(2).showTrail = true;
bodies(2).name = 'w';

% define the tyre object primitve
bodies(3).tfs = tf_t;
bodies(3).shapePrimitiveType = 'cylinder';
bodies(3).shapePrimitiveParams = [simparams(4) 0.5];
bodies(3).shapeColor = [0.5 0.1 0.1 0.1];
bodies(3).showCS = true;
bodies(3).showTrail = true;
bodies(3).name = 't';

%% Configure and Run the Simulation Visualization

% Configure the visualizer's frame-rate
simvizconf.viewdim = '2D';
simvizconf.viewrange = [-3 3; -0.5 3.5];
simvizconf.fps = 30.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.4;
simvizconf.csfontsize = [];
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 24;
simvizconf.Print = [];
%simvizconf.Print = 'pdf';


% Run the visualizers
mbSimulationVisualizer(bodies, simparams, simresult, simtime, simvizconf);

%% Simulation Results Overivew

simovconf.FontSize = 24;
simovconf.DataSet = [2 5; 8 11];
%simovconf.Print = [];
simovconf.Print = 'pdf';

mbSimulationOverview(simtime,simresult,[q Dq],simovconf);

%%
% EOF
