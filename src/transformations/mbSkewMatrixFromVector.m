%
%   function skew_matrix = mbSkewMatrixFromVector (vector)
%
%   This function changes the representation of a 3D vector to
%   skew-symmetric matrix.
%
%   inputs:
%       vector          Input 3D vector.
%
%   returns:
%       skew_matrix     Skew-symmetric matrix representation of  vector in
%                       R^3.
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           9/6/2016
%
%   Copyrigth(C) 2016, Vassilios Tsounis
%

function skew_matrix = mbSkewMatrixFromVector(vector)

    skew_matrix = [0 -vector(3) vector(2); vector(3) 0 -vector(1); -vector(2) vector(1) 0;];

end
