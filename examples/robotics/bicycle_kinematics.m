%%
%   File:           omnidrive_kinematics.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           8/12/2016
%
%   Desciption:     This script demonstrates a derivation of the
%                   non-holonomic kinematic constraints exhibited by
%                   wheeled systems. 
%

% Optional calls for workspace resetting/cleaning
close all; clear; clc;

%%

% Geometric parameters
syms l_1 l_2 R_1 R_2 real;
syms alpha_1 alpha_2 beta_off1 beta_off2 real;

% Define the offset parameters
pWheelOffsets   = [l_1 l_2].'; 
pWheelRadii     = [R_1 R_2].';
pOffsetAngles   = [alpha_1 alpha_2].';
pSteeringOffsets = [beta_off1 beta_off2].';

% Define the system parameter array
sysparams   = [ pWheelOffsets;
                pWheelRadii;
                pOffsetAngles;
                pSteeringOffsets].';
            
% Set the parameter values
simWheelOffsets   = [0 1].'; 
simWheelRadii     = [0.3 0.3].';
simOffsetAngles   = [0 0].';
simSteeringOffsets = [pi/2 pi/2].';          
            
% Set the simulation parameters
simparams   = [ simWheelOffsets;
                simWheelRadii;
                simOffsetAngles;
                simSteeringOffsets].';

%% Coordinate Systems

% Absolute positions and velocities
syms x_B y_B theta_B real;
syms Dx_B Dy_B Dtheta_B real;

% Velocity of base in local frame
syms Dx_R Dy_R real;

% Wheel steering positions and velocities
syms beta_1 beta_2 real;
syms Dbeta_1 Dbeta_2 real;

% Wheel joint positions and velocities
syms phi_1 phi_2 real;
syms Dphi_1 Dphi_2 real;

% Create trajectory generator functions
syms x_C y_C z_C Dx_C Dy_C Dz_C real;
r_C = [x_C y_C z_C].';
v_C = [Dx_C Dy_C Dz_C].';

% Define the robot state vectors

xi_I = [x_B y_B theta_B].';
phi = [phi_1 phi_2].';
beta_s = [beta_1 beta_2].';

Dxi_I = [Dx_B Dy_B Dtheta_B].';
Dphi = [Dphi_1 Dphi_2].';
Dbeta_s = [Dbeta_1 Dbeta_2].';

%% Generalized Planar Wheeled Robot Equations

%%%
%   For a planar robot with N wheels: 
%
%   [ J_1(beta_s) ] * R_theta * Dxi = [J_2] * Dphi
%   [ C_1(beta_s) ]                   [ 0 ]
%
%   Where:
%
%   1.  Dxi is [ Dx Dy Dtheta ]^T, i.e. the floating-base part of the
%       generalized velocity vector.
%
%   2.  theta is the heading angle of the robot, i.e. the orientation of
%       the B-frame w.r.t the I-frame.
%
%   3.  R_z() is the rotation matrix for rotations about the z-axis
%
%   4.  Dphi is the wheel joint DoFs part of the generalized velocity 
%       vector.
%
%   5.  beta_s is the vector of steering joint angle DoFs part of the
%       generalized coordinate vector.
%
%   6.  J_2 = diag(R_1, R_2, ..., R_n), i.e. the diagonal matrix of the
%       wheel radii.
%
%   7.  J_1 is the rolling-constraints matrix. This is also organized in
%       such a way as to separate the steered and unsteered wheels:
%
%   J_1 = [ J_1f         ] -> the rolling constraints for fixed wheels.
%         [ J_1s(beta_s) ] -> the rolling constraints for steered wheels.
%
%   8.  C_1 is the no-slip constriant matrix. This is also organized in
%       such a way to sparate steered and unsteered wheels:
%
%   C_1 = [ C_1f         ] -> the rolling constraints for fixed wheels.
%         [ C_1s(beta_s) ] -> the rolling constraints for steered wheels.
%
%%%

R_theta = [ cos(theta_B) sin(theta_B) 0;
            -sin(theta_B) cos(theta_B) 0;
            0 0 1];

J_2 = [ R_1 0;
        0 R_2];
    
J_1 = [ sin(alpha_1 + beta_1 + beta_off1) -cos(alpha_1 + beta_1 + beta_off1) -l_1*cos(beta_1 + beta_off1);
        sin(alpha_2 + beta_2 + beta_off2) -cos(alpha_2 + beta_2 + beta_off2) -l_2*cos(beta_2 + beta_off2)];

C_1 = [ cos(alpha_1 + beta_1 + beta_off1) sin(alpha_1 + beta_1 + beta_off1) l_1*sin(beta_1 + beta_off1);
        cos(alpha_2 + beta_2 + beta_off2) sin(alpha_2 + beta_2 + beta_off2) l_2*sin(beta_2 + beta_off2)];

J_C = [J_1; C_1];
J_W = [J_2; zeros(2,2)];

%% Forward & Inverse Differential Kinematics 

% We express the forward kinematics Jacobian
J_F = R_theta.'*(inv(J_C.'*J_C)*J_C.')*J_W;

% We express the forward kinematics Jacobian
J_I = (inv(J_W.'*J_W)*J_W.')*J_C * R_theta;

% Define functions to compute numerical values
func_J_F = matlabFunction(J_F, 'Vars', {[xi_I; beta_s; phi].', sysparams});
func_J_I = matlabFunction(J_I, 'Vars', {[xi_I; beta_s; phi].', sysparams});

%% Kinematics for Multi-Body System

r_IB_inI = [xi_I(1) xi_I(2) 0].';
r_BS1_inS1 = [l_1 0 0].';
r_BS2_inS2 = [l_2 0 0].';
r_SW1_inW1 = [0 0 0].';
r_SW2_inW2 = [0 0 0].';

r_W1_inW1 = [1 0 0].';
r_W2_inW2 = [1 0 0].';

% CS-Frame Orientation Transformations

R_IB = [ cos(theta_B) -sin(theta_B) 0;
         sin(theta_B)  cos(theta_B) 0;
         0 0 1];

R_BS1 = [ cos(alpha_1) -sin(alpha_1) 0;
          sin(alpha_1)  cos(alpha_1) 0;
          0 0 1];
     
R_SW1 = [ cos(beta_1+beta_off1) -sin(beta_1+beta_off1) 0;
          sin(beta_1+beta_off1)  cos(beta_1+beta_off1) 0;
          0 0 1];
      
R_WW1 = [ 1 0 0;
         0 cos(phi_1) -sin(phi_1);
         0 sin(phi_1) cos(phi_1)];
      
R_BS2 = [ cos(alpha_2) -sin(alpha_2) 0;
          sin(alpha_2)  cos(alpha_2) 0;
          0 0 1];
     
R_SW2 = [ cos(beta_2+beta_off2) -sin(beta_2+beta_off2) 0;
          sin(beta_2+beta_off2)  cos(beta_2+beta_off2) 0;
          0 0 1];
      
R_WW2 = [ 1 0 0;
         0 cos(phi_2) -sin(phi_2);
         0 sin(phi_2) cos(phi_2)];
    
% Frame and body positions
r_IW1_inI = r_IB_inI + R_IB*R_BS1*r_BS1_inS1 + R_IB*R_BS1*R_SW1*r_SW1_inW1;
r_IW2_inI = r_IB_inI + R_IB*R_BS2*r_BS2_inS2 + R_IB*R_BS2*R_SW2*r_SW2_inW2;

% Compute the total rotation matrices, interpreted as cosine-matrices
C_W1 = R_IB*R_BS1*R_SW1*R_WW1;
C_W2 = R_IB*R_BS2*R_SW2*R_WW2;

% Compute angle-axis
[u_W1, theta_W1] = mbRotationMatrixToAxisAngle(C_W1);
[u_W2, theta_W2] = mbRotationMatrixToAxisAngle(C_W2);

% Poses
pose_B_inI  = [[xi_I(1) xi_I(2) 0] [0 0 1 xi_I(3)]].';
pose_IW1_inI = [r_IW1_inI.' u_W1.' theta_W1].';
pose_IW2_inI = [r_IW2_inI.' u_W2.' theta_W2].';

%% Multi-Rigid-Body Kinematics Generator Functions

tf_B    = matlabFunction(pose_B_inI, 'Vars', {[xi_I; beta_s; phi; Dxi_I; Dphi; r_C; v_C].', sysparams});
tf_W1   = matlabFunction(pose_IW1_inI, 'Vars', {[xi_I; beta_s; phi; Dxi_I; Dphi; r_C; v_C].', sysparams});
tf_W2   = matlabFunction(pose_IW2_inI, 'Vars', {[xi_I; beta_s; phi; Dxi_I; Dphi; r_C; v_C].', sysparams});

tf_ICR  = @(data,params) [data(12) data(13) data(14) 0 0 1 0].';
tf_G    = @(data,params) [data(8) data(9) data(10) 0 0 1 data(11)].';

%% Trajectory Following using Inverse Differential Kinematics

% Initial state of the robot
xi_0 = [1 1 pi/1.8].';
Dxi_0 = zeros(3,1);

% Inital state of the actuators
Nw = numel(phi);
phi_0 = zeros(Nw,1);
Dphi_0 = zeros(Nw,1);
Ns = numel(beta_s);
beta_s_0 = zeros(Ns,1);
Dbeta_s_0 = zeros(Ns,1);

% Controller mode select
opt_controller_mode = 'polar_kinematic';

% Configure the controller parmeters
K_p = 5;
K_rho = 2;
K_alpha = 5;
K_beta = -0.6;
Kp_steering = 2;
omega_min_threshold = 1e-4;

% Desired trajectory parameters
traj_circle_center = [4 4 0].';
traj_circle_radius = 3;
traj_circle_freq = 0.1;

r_goal = @(t) traj_circle_center + traj_circle_radius*[sin(2*pi*traj_circle_freq*t), cos(2*pi*traj_circle_freq*t), 0.1]';
v_goal = @(t) (2*pi) * (traj_circle_freq * traj_circle_radius) * [cos(2*pi*traj_circle_freq*t),0,-sin(2*pi*traj_circle_freq*t)]';
theta_goal = @(t) 2*pi*traj_circle_freq*t;

% Define here the time resolution
deltaT = 0.01;
time_data = 0:deltaT:(2/traj_circle_freq);

% q, r, and r_goal are stored for every point in time in the following arrays
q_data = zeros(3+Nw+Ns+4+4,length(time_data));
Dq_data = zeros(3+Nw+Ns+4+4,length(time_data));
beta_s_data = zeros(Ns,length(time_data));
Dbeta_s_data = zeros(Ns,length(time_data));

% Initialize varables
xi_iter = xi_0;
Dxi_iter = Dxi_0;
phi_iter = phi_0;
Dphi_iter = Dphi_0;
beta_s_iter = beta_s_0;
beta_s_iter_new = beta_s_0;
Dbeta_s_iter = Dbeta_s_0;
r_icr_iter = [0 0 0].';
R_iter = 0;

% Run the numerical simulation
for i = 1:length(time_data)
    
    % Update step
    t = time_data(i);
    r_goal_iter = r_goal(t);
    theta_goal_iter = theta_goal(t);
    xi_iter = xi_iter + Dxi_iter*deltaT;
    phi_iter = phi_iter + Dphi_iter*deltaT;
    beta_s_iter = beta_s_iter + Dbeta_s_iter*deltaT;
    r_iter = [xi_iter(1) xi_iter(2) 0].';
    theta_iter = xi_iter(3);
    
    % Compute current Jacobians from sensor data     
    J_I_iter = func_J_I([xi_iter; beta_s_iter; phi_iter].', simparams);
    J_F_iter = func_J_F([xi_iter; beta_s_iter; phi_iter].', simparams);
    
    % Accumulate the new state from the forward kinematics
    Dxi_iter = J_F_iter*Dphi_iter;
    
    % Generate the next target action from the inverse kinematics
    switch opt_controller_mode
        
        case 'position_only' 
            v_iter = K_p*(r_goal_iter - r_iter);
            Dphi_iter = J_I_iter*v_iter;
            
        case 'polar_kinematic'
            
            % Base motion controller
            rho = sqrt((r_goal_iter(1)-r_iter(1))^2+(r_goal_iter(2)-r_iter(2))^2); 
            lambda = atan2(r_goal_iter(2)-r_iter(2), r_goal_iter(1)-r_iter(1));
            alpha = lambda - xi_iter(3);
            alpha = mod(alpha+pi, 2*pi) - pi;
            beta = - xi_iter(3) - alpha;
            beta = mod(beta+pi, 2*pi) - pi;
            vu = K_rho * rho;
            omega = K_alpha * alpha + K_beta * beta;
            
            % Steering Motion controllers
            if abs(omega) >= omega_min_threshold
                beta_s_iter_new(1) = 0;
                R_iter = vu/omega;
                beta_s_iter_new(2) = pi/2 - atan2(1,(R_iter-simWheelOffsets(2)));
                %beta_s_iter_new(2) = mod(beta_s_iter_new(2)+pi, 2*pi) - pi;
                r_icr_iter = r_iter + [-R_iter*sin(theta_iter); R_iter*cos(theta_iter); 0];
            else
                beta_s_iter_new = zeros(Ns,1);
            end
            Dbeta_s_iter = Kp_steering * (beta_s_iter_new - beta_s_iter);
            
            % Wheel motion controllers
            v_iter = [cos(xi_iter(3)) 0; sin(xi_iter(3)) 0; 0 1] * [vu; omega];
            Dphi_iter = J_I_iter*v_iter;
            
        otherwise
            error('Error: Incorrect controller mode has been specified.');
    end
    
    
    % Logging
    q_data(:,i) = [xi_iter; beta_s_iter; phi_iter; r_goal_iter; theta_goal_iter; r_icr_iter; R_iter];
    Dq_data(:,i) = [Dxi_iter; Dbeta_s_iter; Dphi_iter; [0 0 0].'; 0; [0 0 0].'; 0];
    beta_s_data(:,i) = beta_s_iter;
    Dbeta_s_data(:,i) = Dbeta_s_iter;
end

%%
% 
% simovconf.FontSize = 18;
% simovconf.DataSet = [1 2; 3 4];
% mbSimulationOverview(time_data,[q_data(6:7,:); Dq_data(6:7,:)],[phi; Dphi].',simovconf);
% 
% %%

% simovconf.FontSize = 18;
% simovconf.DataSet = [1 2; 3 4];
% mbSimulationOverview(time_data,[beta_s_data; Dbeta_s_data],[beta_s; Dbeta_s].',simovconf);
% 
% %%
% 
% simovconf.FontSize = 18;
% simovconf.DataSet = [1; 2];
% mbSimulationOverview(time_data,[q_data(3,:); Dq_data(3,:)],[xi_I(3); Dxi_I(3)].',simovconf);
% 
% %%
% 
% syms R_ICR real;
% simovconf.FontSize = 18;
% simovconf.DataSet = [];
% mbSimulationOverview(time_data,q_data(15,:),R_ICR,simovconf);

%% Define Visualization Environment

% Base
bodies(1).tfs = tf_B;
bodies(1).shapePrimitiveType = 'parallelogram';
bodies(1).shapePrimitiveParams = [1.0 0.1 0.1];
bodies(1).shapeOffsets = [0.5 0 0 0 0 0].';
bodies(1).shapeColor = [0.5 0.6 0.6 0.6]; 
bodies(1).showCS = true;
bodies(1).showTrail = true;
bodies(1).name = 'B';

% Wheels
bodies(2).tfs = tf_W1;
bodies(2).shapePrimitiveType = 'cylinder';
bodies(2).shapePrimitiveParams = [simWheelRadii(1) 0.05];
bodies(2).shapeColor = [0.8 0.3 0.3 0.3];
bodies(2).shapeOffsets = [0 0 0 0 pi/2 0].';
bodies(2).showCS = true;
bodies(2).showTrail = false;
bodies(2).name = 'W1';

bodies(3).tfs = tf_W2;
bodies(3).shapePrimitiveType = 'cylinder';
bodies(3).shapePrimitiveParams = [simWheelRadii(2) 0.05];
bodies(3).shapeColor = [0.8 0.3 0.3 0.3];
bodies(3).shapeOffsets = [0 0 0 0 pi/2 0].';
bodies(3).showCS = true;
bodies(3).showTrail = false;
bodies(3).name = 'W2';

% The target frame
bodies(4).tfs = tf_G;
bodies(4).shapePrimitiveType = 'none'; 
bodies(4).showCS = true;
bodies(4).showTrail = true;
bodies(4).name = 'G';

% % The target frame
% bodies(5).tfs = tf_ICR;
% bodies(5).shapePrimitiveType = 'none'; 
% bodies(5).showCS = false;
% bodies(5).showTrail = true;
% bodies(5).name = 'ICR';

%% Run the Simulation Visualization

% Configure visualization data
simdata = [q_data.' Dq_data.'].';
simtime = time_data;

% Configure the visualizer's frame-rate
simvizconf.viewdim = '3D';
simvizconf.viewrange = [-0.5 8.5; -0.5 8.5; -0.5 2.5];
simvizconf.fps = 20.0;  
simvizconf.camlight = false;
simvizconf.recordmode = false;
simvizconf.limitrate = true;
simvizconf.csscale = 0.3;
simvizconf.csfontsize = 18;
simvizconf.debugmode = false;
simvizconf.WaitPrompt = true;
simvizconf.FontSize = 18;

% Run the visualizers
mbSimulationVisualizer(bodies, simparams, simdata, simtime, simvizconf);

%% Simulation Overivew

simovconf.FontSize = 18;
simovconf.DataSet = [1 2; 8 9];
mbSimulationOverview(simtime,simdata([1:7 16:23],:),[xi_I; beta_s; phi; Dxi_I; Dbeta_s; Dphi].',simovconf);

%% 
% EOF
